#include <3dmr/render/viewer.h>
#include "shell.h"

#ifndef TDSH_CALLBACKS_H
#define TDSH_CALLBACKS_H

extern int running;

void callback_mouse(struct Viewer* viewer, double xpos, double ypos, double dx, double dy, int buttonLeft, int buttonMiddle, int buttonRight, void* userData);
void callback_wheel(struct Viewer* viewer, double xoffset, double yoffset, void* userData);
void callback_key(struct Viewer* viewer, int key, int scancode, int action, int mods, void* userData);
void callback_resize(struct Viewer* viewer, void* userData);
void callback_close(struct Viewer* viewer, void* userData);

int shell_make_callbacks(struct DemoShell* shell);

#endif
