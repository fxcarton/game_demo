#include "shell.h"

#ifndef TDSH_MESH_H
#define TDSH_MESH_H

int shell_make_mesh_functions(struct DemoShell* shell);

int variable_is_mesh(const struct Variable* var);
struct Mesh* variable_to_mesh(struct Variable* var);
int variable_make_mesh(struct DemoShell* shell, struct Variable* ret);

#endif
