#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eval.h"
#include "parser.h"
#include "operators.h"
#include "completion.h"
#include "userfunc.h"

struct UserVar {
    struct Variable var;
    char* name;
};

static void var_user_destroy(struct DemoShell* shell, void* d) {
    if (!d) return;
    variable_free(&((struct UserVar*)d)->var);
    free(((struct UserVar*)d)->name);
    free(d);
}

static int var_user_get(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    return variable_copy(&((struct UserVar*)r->data)->var, dest);
}

static int var_user_set(const struct Variable* src, void* dest) {
    const struct ShellRefCount* r = dest;
    variable_free(&((struct UserVar*)r->data)->var);
    return variable_copy(src, &((struct UserVar*)r->data)->var);
}

static int var_user_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.gdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}

static void var_user_free(struct Variable* v) {
    shell_decref(v->data.dpointer.gdata);
}

static const struct PointerInfo userVar = {var_user_get, var_user_set, var_user_copy, var_user_free};

static int var_new_get(const void* src, struct Variable* dest) {
    fprintf(stderr, "Error: undefined pointer\n"); /* should not happen */
    return 0;
}

static int var_new_set(const struct Variable* src, void* dest) {
    struct ShellRefCount* r = dest;
    struct UserVar* u = r->data;
    struct Variable* ptr;
    if (!variable_copy(src, &u->var)) return 0;
    if (shell_incref(r)) {
        if ((ptr = shell_add_variable(r->shell, u->name))) {
            variable_make_ptr(&userVar, (void*)r, (void*)r, ptr);
            return 1;
        }
        shell_decref(r);
    }
    variable_free(&u->var);
    u->var.type = VOID;
    return 0;
}

static int var_new_copy(const struct Variable* src, struct Variable* dest) {
    return 0;
}

static void var_new_free(struct Variable* v) {
    shell_decref(v->data.dpointer.gdata);
}

static const struct PointerInfo newVar = {var_new_get, var_new_set, var_new_copy, var_new_free};

static int push(void*** stack, unsigned int* count, unsigned int* alloc, void* val) {
    void** tmp;
    if (*count + 1 <= *alloc) {
        (*stack)[(*count)++] = val;
        return 1;
    } else if (*alloc + 1 == ((unsigned int)-1)) {
        fprintf(stderr, "Error: stack too large\n");
        return 0;
    } else if ((tmp = realloc(*stack, (*count + 1) * sizeof(void*)))) {
        *stack = tmp;
        tmp += (*count)++;
        *tmp = val;
        (*alloc) = (*count);
        return 1;
    }
    fprintf(stderr, "Error: memory allocation failed\n");
    return 0;
}

static int push_node(struct EvalState* state, const struct AstNode* node) {
    struct AstNode* n;
    if (!(n = malloc(sizeof(*n)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        return 0;
    }
    if (!push((void***)&state->operators, &state->numOps, &state->allocOps, n)) {
        free(n);
        return 0;
    }
    *n = *node;
    switch (n->type) {
        case BLOC: n->data.bloc.open = 0; break;
        case TUPLE: n->data.tuple.open = 0; break;
        case CONTROL_FLOW: if (n->data.controlFlow.type != IF) n->data.controlFlow.elseBody = NULL; break;
        default:;
    }
    return 1;
}

int _eval_push_node(struct EvalState* state, const struct AstNode* node) {
    return push_node(state, node);
}

static void pop_node(struct EvalState* state) {
    free(state->operators[--(state->numOps)]);
}

static int push_expr(struct EvalState* state) {
    struct Variable* v;
    if (!(v = malloc(sizeof(*v)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        return 0;
    }
    if (!push((void***)&state->expressions, &state->numExprs, &state->allocExprs, v)) {
        free(v);
        return 0;
    }
    v->type = VOID;
    return 1;
}

static void pop_expr(struct EvalState* state) {
    variable_free(state->expressions[--(state->numExprs)]);
    free(state->expressions[state->numExprs]);
}

int eval_node_start(const struct AstNode* root, struct DemoShell* shell, struct EvalState* state) {
    state->shell = shell;
    state->expressions = NULL;
    state->operators = NULL;
    state->numExprs = 0;
    state->numOps = 0;
    state->allocExprs = 0;
    state->allocOps = 0;
    return state->ok = push_node(state, root);
}

int eval_node_iter(struct EvalState* state) {
    struct AstNode* cur;
    struct Variable *var, tmpVar;
    long condition;
    size_t i;

    if (!(state->ok && state->numOps)) return 0;
    cur = state->operators[state->numOps - 1];
    switch (cur->type) {
        case BLOC:
            if (cur->data.bloc.count) {
                if (cur->data.bloc.open) {
                    if (state->numExprs) pop_expr(state);
                }
                state->ok = push_node(state, *(cur->data.bloc.list)++);
                cur->data.bloc.count--;
                cur->data.bloc.open = 1;
            } else {
                if (!cur->data.bloc.open) {
                    state->ok = push_expr(state);
                }
                pop_node(state);
            }
            break;
        case CONTROL_FLOW:
            switch (cur->data.controlFlow.type) {
                case IF:
                    if (cur->data.controlFlow.condition) {
                        state->ok = push_node(state, cur->data.controlFlow.condition);
                        cur->data.controlFlow.condition = NULL;
                    } else if (cur->data.controlFlow.body) {
                        if (!state->numExprs || !variable_to_int(state->expressions[state->numExprs - 1], &condition)) {
                            fprintf(stderr, "Error: condition should evaluate to int\n");
                            state->ok = 0;
                        } else {
                            pop_expr(state);
                            if (condition) {
                                state->ok = push_node(state, cur->data.controlFlow.body);
                            } else if (cur->data.controlFlow.elseBody) {
                                state->ok = push_node(state, cur->data.controlFlow.elseBody);
                            } else {
                                state->ok = push_expr(state);
                            }
                            cur->data.controlFlow.body = NULL;
                        }
                    } else {
                        pop_node(state);
                    }
                    break;
                case FOR:
                    if (cur->data.controlFlow.condition->type != BLOC
                     || !cur->data.controlFlow.condition->data.bloc.open
                     || cur->data.controlFlow.condition->data.bloc.count != 3) {
                        state->ok = 0;
                        break;
                    } else if (!cur->data.controlFlow.elseBody) {
                        state->ok = push_node(state, cur->data.controlFlow.condition->data.bloc.list[1])
                                 && push_node(state, cur->data.controlFlow.condition->data.bloc.list[0]);
                        cur->data.controlFlow.elseBody = cur->data.controlFlow.condition->data.bloc.list[0];
                    } else {
                        if (!state->numExprs || !variable_to_int(state->expressions[state->numExprs - 1], &condition)) {
                            fprintf(stderr, "Error: condition should evaluate to int\n");
                            state->ok = 0;
                        } else {
                            pop_expr(state); /* condition */
                            pop_expr(state); /* init or incr */
                            if (cur->data.controlFlow.elseBody == cur) {
                                pop_expr(state); /* body */
                            } else {
                                cur->data.controlFlow.elseBody = cur;
                            }
                            if (condition) {
                                state->ok = push_node(state, cur->data.controlFlow.condition->data.bloc.list[1])
                                         && push_node(state, cur->data.controlFlow.condition->data.bloc.list[2])
                                         && push_node(state, cur->data.controlFlow.body);
                            } else {
                                pop_node(state);
                                state->ok = push_expr(state);
                            }
                        }
                    }
                    break;
                case WHILE:
                    if (!cur->data.controlFlow.elseBody) {
                        state->ok = push_node(state, cur->data.controlFlow.condition)
                                 && push_expr(state);
                        cur->data.controlFlow.elseBody = cur;
                    } else {
                        if (!state->numExprs || !variable_to_int(state->expressions[state->numExprs - 1], &condition)) {
                            fprintf(stderr, "Error: condition should evaluate to int\n");
                            state->ok = 0;
                        } else {
                            pop_expr(state); /* condition */
                            pop_expr(state); /* VOID or body */
                            if (condition) {
                                state->ok = push_node(state, cur->data.controlFlow.condition)
                                         && push_node(state, cur->data.controlFlow.body);
                            } else {
                                pop_node(state);
                                state->ok = push_expr(state);
                            }
                        }
                    }
                    break;
                default:
                    fprintf(stderr, "Error: unsupported control flow type\n");
                    state->ok = 0;
            }
            break;
        case IDENTIFIER:
            if (!push_expr(state)) {
                state->ok = 0;
            } else if (!(var = shell_get_variable(state->shell, cur->data.identifier.name, !cur->data.identifier.isDecl))) {
                struct ShellRefCount* r;
                struct UserVar* u;
                if (!cur->data.identifier.isDecl) {
                    fprintf(stderr, "Error: %s not defined\n", cur->data.identifier.name);
                    state->ok = 0;
                } else if (!(u = malloc(sizeof(*u)))
                        || !(u->name = malloc(strlen(cur->data.identifier.name) + 1))
                        || !(r = shell_new_refcount(state->shell, var_user_destroy, u))) {
                    if (u) {
                        free(u->name);
                        free(u);
                    }
                    fprintf(stderr, "Error: failed to create variable %s\n", cur->data.identifier.name);
                    state->ok = 0;
                } else {
                    u->var.type = VOID;
                    strcpy(u->name, cur->data.identifier.name);
                    variable_make_ptr(&newVar, r, r, state->expressions[state->numExprs - 1]);
                    pop_node(state);
                }
            } else {
                if (!variable_copy(var, state->expressions[state->numExprs - 1])) {
                    fprintf(stderr, "Error: failed to copy variable\n");
                    state->ok = 0;
                }
                pop_node(state);
            }
            break;
        case FUNCTION_CALL:
            if (!(var = shell_get_variable(state->shell, cur->data.functionCall.function, 1))) {
                fprintf(stderr, "Error: %s not defined\n", cur->data.identifier.name);
                state->ok = 0;
            } else if (!variable_copy_dereference(var, &tmpVar)) {
                fprintf(stderr, "Error: failed to copy variable\n");
                state->ok = 0;
            } else if (tmpVar.type != FUNCTION) {
                fprintf(stderr, "Error: not a function\n");
                state->ok = 0;
                variable_free(&tmpVar);
            } else if (cur->data.functionCall.args != cur) {
                if (cur->data.functionCall.args) {
                    struct AstNode tmp;
                    tmp.type = CONSTANT;
                    if (cur->data.functionCall.args->type == TUPLE && cur->data.functionCall.args->data.tuple.open) {
                        unsigned int i, nArgs = cur->data.functionCall.args->data.tuple.count;
                        variable_make_int(nArgs, &tmp.data.constant.value);
                        state->ok = push_node(state, &tmp);
                        for (i = 0; state->ok && i < nArgs; i++) {
                            state->ok = push_node(state, cur->data.functionCall.args->data.tuple.list[i]);
                        }
                    } else {
                        variable_make_int(1, &tmp.data.constant.value);
                        state->ok = push_node(state, &tmp)
                                 && push_node(state, cur->data.functionCall.args);
                    }
                    cur->data.functionCall.args = cur;
                } else {
                    if (!push_expr(state)) {
                        state->ok = 0;
                    } else if (!variable_function_call(&tmpVar, state->expressions[state->numExprs - 1], NULL, 0, state)) {
                        state->ok = 0;
                        state->expressions[state->numExprs - 1]->type = VOID;
                    }
                    pop_node(state);
                }
                variable_free(&tmpVar);
            } else {
                struct Variable* args;
                unsigned int i, nArgs;
                if (!state->numExprs || state->expressions[state->numExprs - 1]->type != INT
                 || state->numExprs - 1 < (nArgs = state->expressions[state->numExprs - 1]->data.dint)
                 || !(args = malloc(nArgs * sizeof(*args)))) {
                    state->ok = 0;
                } else {
                    pop_expr(state);
                    for (i = 0; i < nArgs; i++) {
                        args[i] = *(state->expressions[--state->numExprs]);
                        free(state->expressions[state->numExprs]);
                    }
                    if (!push_expr(state)) {
                        state->ok = 0;
                    } else if (!variable_function_call(&tmpVar, state->expressions[state->numExprs - 1], args, nArgs, state)) {
                        state->ok = 0;
                        state->expressions[state->numExprs - 1]->type = VOID;
                    }
                    pop_node(state);
                    while (nArgs) {
                        variable_free(args + (--nArgs));
                    }
                    free(args);
                }
                variable_free(&tmpVar);
            }
            break;
        case BINARY_OP:
            if (cur->data.binaryOp.b) {
                state->ok = push_node(state, cur->data.binaryOp.b)
                         && push_node(state, cur->data.binaryOp.a);
                cur->data.binaryOp.b = NULL;
            } else if (state->numExprs < 2) {
                state->ok = 0;
            } else {
                char* err;
                if (!cur->data.binaryOp.op(state->expressions[state->numExprs - 2], state->expressions[state->numExprs - 1], &tmpVar, &err)) {
                    fprintf(stderr, "Error: %s\n", err ? err : "binary op eval");
                    free(err);
                    state->ok = 0;
                } else {
                    pop_expr(state);
                    variable_free(state->expressions[state->numExprs - 1]);
                    *(state->expressions[state->numExprs - 1]) = tmpVar;
                    pop_node(state);
                }
            }
            break;
        case UNARY_OP:
            if (cur->data.unaryOp.v) {
                state->ok = push_node(state, cur->data.unaryOp.v);
                cur->data.unaryOp.v = NULL;
            } else if (!state->numExprs) {
                state->ok = 0;
            } else {
                char* err;
                if (!cur->data.unaryOp.op(state->expressions[state->numExprs - 1], &tmpVar, &err)) {
                    fprintf(stderr, "Error: %s\n", err ? err : "unary op eval");
                    free(err);
                    state->ok = 0;
                } else {
                    variable_free(state->expressions[state->numExprs - 1]);
                    *(state->expressions[state->numExprs - 1]) = tmpVar;
                    pop_node(state);
                }
            }
            break;
        case ARRAY_ACCESS:
            if (cur->data.arrayAccess.array) {
                state->ok = push_node(state, cur->data.arrayAccess.array);
                cur->data.arrayAccess.array = NULL;
            } else if (cur->data.arrayAccess.index) {
                state->ok = push_node(state, cur->data.arrayAccess.index);
                cur->data.arrayAccess.index = NULL;
            } else if (state->numExprs < 2) {
                state->ok = 0;
            } else {
                char* err;
                if (!variable_array_member(state->expressions[state->numExprs - 2], state->expressions[state->numExprs - 1], &tmpVar, &err)) {
                    fprintf(stderr, "Error: %s\n", err ? err : "array access");
                    free(err);
                    state->ok = 0;
                } else {
                    pop_expr(state);
                    variable_free(state->expressions[state->numExprs - 1]);
                    *(state->expressions[state->numExprs - 1]) = tmpVar;
                    pop_node(state);
                }
            }
            break;
        case STRUCT_MEMBER:
            if (cur->data.structMember.s) {
                state->ok = push_node(state, cur->data.structMember.s);
                cur->data.structMember.s = NULL;
            } else if (!state->numExprs || cur->data.structMember.member->type != IDENTIFIER) {
                state->ok = 0;
            } else {
                char* err;
                if (!variable_struct_member(state->expressions[state->numExprs - 1], cur->data.structMember.member->data.identifier.name, &tmpVar, &err)) {
                    fprintf(stderr, "Error: %s\n", err ? err : "struct member access");
                    free(err);
                    state->ok = 0;
                } else {
                    variable_free(state->expressions[state->numExprs - 1]);
                    *(state->expressions[state->numExprs - 1]) = tmpVar;
                    pop_node(state);
                }
            }
            break;
        case TUPLE:
            if (cur->data.tuple.count >= 2 && cur->data.tuple.count <= 4)  {
                if (!cur->data.tuple.open) {
                    for (i = cur->data.tuple.count; state->ok && i; i--) {
                        state->ok = push_node(state, cur->data.tuple.list[i - 1]);
                    }
                    cur->data.tuple.open = 1;
                } else {
                    float* ptr;
                    switch (cur->data.tuple.count) {
                        case 2: tmpVar.type = VEC2; ptr = tmpVar.data.dvec2; break;
                        case 3: tmpVar.type = VEC3; ptr = tmpVar.data.dvec3; break;
                        case 4: tmpVar.type = VEC4; ptr = tmpVar.data.dvec4; break;
                        default: state->ok = 0; break;
                    }
                    for (i = 0; state->ok && i < cur->data.tuple.count; i++) {
                        if (!variable_to_float(state->expressions[state->numExprs - cur->data.tuple.count + i], ptr++)) {
                            fprintf(stderr, "Error: invalid comma\n");
                            state->ok = 0;
                        }
                    }
                    for (i = 1; state->ok && i < cur->data.tuple.count; i++) {
                        pop_expr(state);
                    }
                    variable_free(state->expressions[state->numExprs - 1]);
                    *(state->expressions[state->numExprs - 1]) = tmpVar;
                    pop_node(state);
                }
            } else {
                fprintf(stderr, "Error: invalid comma\n");
                state->ok = 0;
            }
            break;
        case CONSTANT:
            if (!push_expr(state)) {
                state->ok = 0;
            } else if (!variable_copy(&cur->data.constant.value, state->expressions[state->numExprs - 1])) {
                fprintf(stderr, "Error: failed to copy variable\n");
                state->ok = 0;
            } else {
                pop_node(state);
            }
            break;
        case FUNCTION_DECL:
            if (!push_expr(state)) {
                state->ok = 0;
            } else if (!userfunc_make(state->shell, cur, state->expressions[state->numExprs - 1])) {
                fprintf(stderr, "Error: failed to create user function\n");
                state->ok = 0;
            } else {
                pop_node(state);
            }
            break;
        default:
            fprintf(stderr, "Error: unsupported node type\n");
            state->ok = 0;
    }
    return (state->ok && state->numOps);
}

int eval_node_end(struct EvalState* state, struct Variable* dest) {
    state->ok = state->ok && (state->numExprs == 1) && (state->numOps == 0);
    if (state->ok) {
        *dest = *(state->expressions[0]);
        free(state->expressions[state->numExprs = 0]);
    }
    while (state->numExprs) pop_expr(state);
    while (state->numOps) pop_node(state);
    free(state->expressions);
    free(state->operators);
    return state->ok;
}
