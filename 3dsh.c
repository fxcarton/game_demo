#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef READLINE
#include <readline/readline.h>
#endif
#include <3dmr/shaders.h>

#include "builtins.h"
#include "mainloop.h"
#include "shell.h"

static void usage(const char* progname) {
    printf("Usage: %s [-s width height] [-c command] [commandFileName]*\n\n", progname);
}

static int parse_args(int argc, char** argv, struct ShellConfig* config) {
    const char* prog = *argv++;
    argc--;
    while (argc > 0) {
        if ((*argv)[0] != '-') break;
        argc--;
        if (!(*argv)[1] || (*argv)[2]) {
            usage(prog);
            return 0;
        }
        if ((*argv)[1] == '-') {
            argv++;
            break;
        }
        switch ((*argv)[1]) {
            case 's':
                if (argc < 2) {
                    goto arg_error;
                }
                config->defWidth = strtoul(*++argv, NULL, 10);
                config->defHeight = strtoul(*++argv, NULL, 10);
                argc -= 2;
                break;
            case 'c':
                if (argc < 1) {
                    goto arg_error;
                }
                config->execCmd = *++argv;
                argc--;
                break;
            arg_error:
            default:
                usage(prog);
                return 0;
        }
        argv++;
    }
    config->argv = argv;
    config->argc = argc >= 0 ? argc : 0;
    return 1;
}

int main(int argc, char** argv) {
    struct DemoShell shell;
    struct ShellConfig config;
    int ret = 0, shellInit = 0;
    const char* shaderPath;

    if ((shaderPath = getenv("DEMO_SHADERS_PATH"))) {
        tdmrShaderRootPath = shaderPath;
    }

#ifdef READLINE
    rl_initialize(); /* do this before starting threads to avoid data races */
#endif
    shell_config_default(&config);
    if (!parse_args(argc, argv, &config)) {
        ret = 1;
    } else if (!(shellInit = shell_init(&shell, &config))) {
        fprintf(stderr, "Error: init shell failed\n");
        ret = 1;
    } else if (!shell_make_builtins(&shell)) {
        fprintf(stderr, "Error: failed to create builtin variables\n");
    } else {
        ret = !shell_main_loop(&shell);
#ifdef READLINE
        rl_cleanup_after_signal(); /* restore tty state in case we interrupted readline() */
#endif
    }

    if (shellInit) shell_free(&shell);
    return ret;
}
