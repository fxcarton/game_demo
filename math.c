#include <stdio.h>
#include <string.h>
#include <math.h>
#include <3dmr/math/interp.h>
#include "builtins.h"
#include "shell.h"

#define vec(n) \
static int vec##n##_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    unsigned int i; \
    if (nArgs == 0) { \
        for (i = 0; i < n; i++) { \
            ret->data.dvec##n[i] = 0; \
        } \
    } else if (nArgs == 1) { \
        if (!variable_to_vec##n(args, ret->data.dvec##n)) { \
            fprintf(stderr, "Error: cannot construct vec" #n " with given argument\n"); \
            return 0; \
        } \
    } else if (nArgs == n) { \
        for (i = 0; i < n; i++) { \
            if (!variable_to_float(args + i, &ret->data.dvec##n[i])) { \
                fprintf(stderr, "Error: cannot construct vec" #n " with argument %u\n", i); \
                return 0; \
            } \
        } \
    } else { \
        fprintf(stderr, "Error: vec" #n " needs 0, 1 or " #n " arguments\n"); \
        return 0; \
    } \
    ret->type = VEC##n; \
    return 1; \
}

vec(2)
vec(3)
vec(4)

#define mat(n) \
static int mat##n##_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    unsigned int i, j; \
    if (nArgs == 0) { \
        for (i = 0; i < n; i++) { \
            for (j = 0; j < n; j++) { \
                ret->data.dmat##n[i][j] = 0; \
            } \
        } \
    } else if (nArgs == 1) { \
        Vec##n diag; \
        if (!variable_to_vec##n(args, diag)) { \
            fprintf(stderr, "Error: cannot construct mat" #n " with given argument\n"); \
            return 0; \
        } \
        for (i = 0; i < n; i++) { \
            for (j = 0; j < n; j++) { \
                ret->data.dmat##n[i][j] = (i == j) ? diag[i] : 0; \
            } \
        } \
    } else if (nArgs == n) { \
        for (i = 0; i < n; i++) { \
            if (!variable_to_float(args + i, &ret->data.dmat##n[i][i])) { \
                break; \
            } \
        } \
        if (i == n) { \
            for (i = 0; i < n; i++) { \
                for (j = 0; j < n; j++) { \
                    if (i != j) ret->data.dmat##n[i][j] = 0; \
                } \
            } \
        } else { \
            for (i = 0; i < n; i++) { \
                if (!variable_to_vec##n(args + i, ret->data.dmat##n[i])) { \
                    break; \
                } \
            } \
            if (i != n) { \
                fprintf(stderr, "Error: cannot construct mat" #n " with given arguments\n"); \
                return 0; \
            } \
        } \
    } else if (nArgs == n * n) { \
        for (i = 0; i < n; i++) { \
            for (j = 0; j < n; j++) { \
                if (!variable_to_float(args++, &ret->data.dmat##n[i][j])) { \
                    fprintf(stderr, "Error: cannot construct mat" #n " with argument %u\n", n * i + j); \
                    return 0; \
                } \
            } \
        } \
    } else { \
        fprintf(stderr, "Error: mat" #n " needs 0, 1, " #n " or " #n "x" #n " arguments\n"); \
        return 0; \
    } \
    ret->type = MAT##n; \
    return 1; \
}

mat(2)
mat(3)
mat(4)

static int quaternion(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    unsigned int i;
    if (nArgs == 0) {
        quaternion_load_id(ret->data.dquaternion);
    } else if (nArgs == 1) {
        if (!variable_to_quaternion(args, ret->data.dquaternion)) {
            fprintf(stderr, "Error: cannot construct quaternion with given argument\n");
            return 0;
        }
    } else if (nArgs == 2) {
        Vec3 axis;
        float angle;
        if (!variable_to_vec3(args, axis) || !variable_to_float(args + 1, &angle)) {
            fprintf(stderr, "Error: expected axis (vec3) and angle (float)\n");
            return 0;
        }
        quaternion_set_axis_angle(ret->data.dquaternion, axis, angle);
    } else if (nArgs == 4) {
        for (i = 0; i < 4; i++) {
            if (!variable_to_float(args + i, &ret->data.dquaternion[i])) {
                fprintf(stderr, "Error: cannot construct quaternion with argument %u\n", i);
                return 0;
            }
        }
    } else {
        fprintf(stderr, "Error: quaternion needs 0, 1, 2 or 4 arguments\n");
        return 0;
    }
    ret->type = QUATERNION;
    return 1;
}

static int axis(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q;

    if (nArgs != 1 || !variable_to_quaternion(args, q)) {
        fprintf(stderr, "Error: expected one quaternion argument in axis()\n");
        return 0;
    }
    quaternion_get_axis(q, ret->data.dvec3);
    ret->type = VEC3;
    return 1;
}

static int angle(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q;

    if (nArgs != 1 || !variable_to_quaternion(args, q)) {
        fprintf(stderr, "Error: expected one quaternion argument in angle()\n");
        return 0;
    }
    variable_make_float(quaternion_get_angle(q), ret);
    return 1;
}

#define rot(n) \
static int rot##n(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    Vec3 axis; \
    float angle; \
 \
    if (nArgs == 1) { \
        if (!variable_to_quaternion(args, ret->data.dquaternion)) { \
            fprintf(stderr, "Error: cannot construct rot" #n " with given argument\n"); \
            return 0; \
        } \
        quaternion_get_axis(ret->data.dquaternion, axis); \
        angle = quaternion_get_angle(ret->data.dquaternion); \
    } else if (nArgs == 2) { \
        if (!variable_to_vec3(args, axis) || !variable_to_float(args + 1, &angle)) { \
            fprintf(stderr, "Error: expected axis (vec3) and angle (float)\n"); \
            return 0; \
        } \
    } else { \
        fprintf(stderr, "Error: rot" #n " needs 1 quaternion or axis and angle\n"); \
        return 0; \
    } \
    ret->type = MAT##n; \
    load_rot##n(ret->data.dmat##n, axis, angle); \
    return 1; \
}

rot(3)
rot(4)

static int rotation(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec3 axis;
    float angle;
    ret->type = QUATERNION;

    if (nArgs == 1) {
        union Mat {
            Mat3 m3;
            Mat4 m4;
        } m;
        if (variable_to_mat3(args, m.m3)) {
            quaternion_from_mat3(ret->data.dquaternion, MAT_CONST_CAST(m.m3));
            return 1;
        }
        if (variable_to_mat4(args, m.m4)) {
            quaternion_from_mat4(ret->data.dquaternion, MAT_CONST_CAST(m.m4));
            return 1;
        }
    } else if (nArgs == 2) {
        Vec3 u, v;
        if (variable_to_vec3(args, u) && variable_to_vec3(args + 1, v)) {
            compute_rotation(u, v, axis, &angle);
            if (!axis[0] && !axis[1] && !axis[2] && angle) {
                fprintf(stderr, "Error: trying to find rotation between two opposed vectors, solution is not unique (rotation axis can be any normal vector)\n");
                return 0;
            }
            quaternion_set_axis_angle(ret->data.dquaternion, axis, angle);
            return 1;
        }
    }
    fprintf(stderr, "Error: expected two vec3 or a mat3/4 in rotation()\n");
    return 0;
}

static int swing(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q, t;
    Vec3 direction;

    if (nArgs == 2) {
        if (!variable_to_quaternion(args++, q)) {
            fprintf(stderr, "Error: expected quaternion\n");
            return 0;
        }
    } else if (nArgs == 3) {
        Vec3 axis;
        float angle;
        if (!variable_to_vec3(args++, axis) || !variable_to_float(args++, &angle)) {
            fprintf(stderr, "Error: expected axis (vec3) and angle (float)\n");
            return 0;
        }
        quaternion_set_axis_angle(q, axis, angle);
    } else {
        fprintf(stderr, "Error: expected quaternion or axis and angle, and vec3\n");
        return 0;
    }
    if (!variable_to_vec3(args, direction)) {
        fprintf(stderr, "Error: expected quaternion\n");
        return 0;
    }
    quaternion_decompose_swing_twist(q, direction, ret->data.dquaternion, t);
    ret->type = QUATERNION;
    return 1;
}

static int twist(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q, s;
    Vec3 direction;

    if (nArgs == 2) {
        if (!variable_to_quaternion(args++, q)) {
            fprintf(stderr, "Error: expected quaternion\n");
            return 0;
        }
    } else if (nArgs == 3) {
        Vec3 axis;
        float angle;
        if (!variable_to_vec3(args++, axis) || !variable_to_float(args++, &angle)) {
            fprintf(stderr, "Error: expected axis (vec3) and angle (float)\n");
            return 0;
        }
        quaternion_set_axis_angle(q, axis, angle);
    } else {
        fprintf(stderr, "Error: expected quaternion or axis and angle, and vec3\n");
        return 0;
    }
    if (!variable_to_vec3(args, direction)) {
        fprintf(stderr, "Error: expected quaternion\n");
        return 0;
    }
    quaternion_decompose_swing_twist(q, direction, s, ret->data.dquaternion);
    ret->type = QUATERNION;
    return 1;
}

static int normalize(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg;
    int ptr;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in normalize()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &arg);
    switch (arg.type) {
        case VEC2: mul2sv(ret->data.dvec2, 1.0f / norm2(arg.data.dvec2), arg.data.dvec2); break;
        case VEC3: mul3sv(ret->data.dvec3, 1.0f / norm3(arg.data.dvec3), arg.data.dvec3); break;
        case VEC4: mul4sv(ret->data.dvec4, 1.0f / norm4(arg.data.dvec4), arg.data.dvec4); break;
        case QUATERNION: mul4sv(ret->data.dquaternion, 1.0f / norm4(arg.data.dquaternion), arg.data.dquaternion); break;
        default:
            fprintf(stderr, "Error: invalid argument type in normalize()\n");
            if (ptr) variable_free(&arg);
            return 0;
    }
    ret->type = arg.type;
    return 1;
}

static int norm(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg;
    int ptr;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in norm()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &arg);
    switch (arg.type) {
        case VEC2: ret->data.dfloat = norm2(arg.data.dvec2); break;
        case VEC3: ret->data.dfloat = norm3(arg.data.dvec3); break;
        case VEC4: ret->data.dfloat = norm4(arg.data.dvec4); break;
        case QUATERNION: ret->data.dfloat = norm4(arg.data.dquaternion); break;
        default:
            fprintf(stderr, "Error: invalid argument type in norm()\n");
            if (ptr) variable_free(&arg);
            return 0;
    }
    ret->type = FLOAT;
    return 1;
}

static int transpose(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    int ptr;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in tr()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, ret);
    switch (ret->type) {
        case MAT2: transpose2m(ret->data.dmat2); break;
        case MAT3: transpose3m(ret->data.dmat3); break;
        case MAT4: transpose4m(ret->data.dmat4); break;
        default:
            fprintf(stderr, "Error: invalid argument type in tr()\n");
            if (ptr) variable_free(ret);
            return 0;
    }
    return 1;
}

static int trace(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg;
    int ptr;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in trace()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &arg);
    switch (arg.type) {
        case MAT2: ret->data.dfloat = arg.data.dmat2[0][0] + arg.data.dmat2[1][1]; break;
        case MAT3: ret->data.dfloat = arg.data.dmat3[0][0] + arg.data.dmat3[1][1] + arg.data.dmat3[2][2]; break;
        case MAT4: ret->data.dfloat = arg.data.dmat4[0][0] + arg.data.dmat4[1][1] + arg.data.dmat4[2][2] + arg.data.dmat4[3][3]; break;
        default:
            fprintf(stderr, "Error: invalid argument type in trace()\n");
            if (ptr) variable_free(&arg);
            return 0;
    }
    ret->type = FLOAT;
    return 1;
}

static int det(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg;
    int ptr;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in det()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &arg);
    switch (arg.type) {
        case MAT2: ret->data.dfloat = det2(MAT_CONST_CAST(arg.data.dmat2)); break;
        case MAT3: ret->data.dfloat = det3(MAT_CONST_CAST(arg.data.dmat3)); break;
        case MAT4: ret->data.dfloat = det4(MAT_CONST_CAST(arg.data.dmat4)); break;
        default:
            fprintf(stderr, "Error: invalid argument type in det()\n");
            if (ptr) variable_free(&arg);
            return 0;
    }
    ret->type = FLOAT;
    return 1;
}

static int inv(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg;
    int ptr, ok;
    if (nArgs != 1) {
        fprintf(stderr, "Error: expected one argument in inv()\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &arg);
    switch (arg.type) {
        case MAT2: ok = invert2m(ret->data.dmat2, MAT_CONST_CAST(arg.data.dmat2)); break;
        case MAT3: ok = invert3m(ret->data.dmat3, MAT_CONST_CAST(arg.data.dmat3)); break;
        case MAT4: ok = invert4m(ret->data.dmat4, MAT_CONST_CAST(arg.data.dmat4)); break;
        default:
            fprintf(stderr, "Error: invalid argument type in inv()\n");
            if (ptr) variable_free(&arg);
            return 0;
    }
    if (!ok) {
        fprintf(stderr, "Error: matrix is not invertible\n");
        return 0;
    }
    ret->type = arg.type;
    return 1;
}

static int dot(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg1, arg2;
    int ptr1, ptr2;
    if (nArgs != 2) {
        fprintf(stderr, "Error: expected two vecs of same size in dot()\n");
        return 0;
    }
    ptr1 = variable_move_dereference(args, &arg1);
    ptr2 = variable_move_dereference(args + 1, &arg2);
    if (arg1.type != arg2.type) {
        fprintf(stderr, "Error: expected two vecs of same size in dot()\n");
        if (ptr1) variable_free(&arg1);
        if (ptr2) variable_free(&arg2);
        return 0;
    }
    switch (arg1.type) {
        case VEC2: ret->data.dfloat = dot2(arg1.data.dvec2, arg2.data.dvec2); break;
        case VEC3: ret->data.dfloat = dot3(arg1.data.dvec3, arg2.data.dvec3); break;
        case VEC4: ret->data.dfloat = dot4(arg1.data.dvec4, arg2.data.dvec4); break;
        default:
            fprintf(stderr, "Error: invalid argument type in dot()\n");
            if (ptr1) variable_free(&arg1);
            if (ptr2) variable_free(&arg2);
            return 0;
    }
    ret->type = FLOAT;
    return 1;
}

static int cross(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable arg1, arg2;
    int ptr1, ptr2;
    if (nArgs != 2) {
        fprintf(stderr, "Error: expected two vec3 in cross()\n");
        return 0;
    }
    ptr1 = variable_move_dereference(args, &arg1);
    ptr2 = variable_move_dereference(args + 1, &arg2);
    if (arg1.type != VEC3 || arg2.type != VEC3) {
        fprintf(stderr, "Error: expected two vec3 in cross()\n");
        if (ptr1) variable_free(&arg1);
        if (ptr2) variable_free(&arg2);
        return 0;
    }
    cross3(ret->data.dvec3, arg1.data.dvec3, arg2.data.dvec3);
    ret->type = VEC3;
    return 1;
}

#define sfunc1(f) \
static int f##_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    float arg; \
    if (nArgs != 1 || !variable_to_float(args, &arg)) { \
        fprintf(stderr, "Error: expected float in " #f "()\n"); \
        return 0; \
    } \
    variable_make_float(f(arg), ret); \
    return 1; \
}
#define sfunc2(f) \
static int f##_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    float arg1, arg2; \
    if (nArgs != 2 || !variable_to_float(args, &arg1) || !variable_to_float(args + 1, &arg2)) { \
        fprintf(stderr, "Error: expected 2 floats in " #f "()\n"); \
        return 0; \
    } \
    variable_make_float(f(arg1, arg2), ret); \
    return 1; \
}
#define sfunc3(f) \
static int f##_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    float arg1, arg2, arg3; \
    if (nArgs != 3 || !variable_to_float(args, &arg1) || !variable_to_float(args + 1, &arg2) || !variable_to_float(args + 2, &arg3)) { \
        fprintf(stderr, "Error: expected 3 floats in " #f "()\n"); \
        return 0; \
    } \
    variable_make_float(f(arg1, arg2, arg3), ret); \
    return 1; \
}
sfunc1(fabs)
sfunc1(sqrt)
sfunc2(pow)
sfunc1(exp)
sfunc1(log)
sfunc1(sin)
sfunc1(cos)
sfunc1(tan)
sfunc1(asin)
sfunc1(acos)
sfunc1(atan)
sfunc2(atan2)
sfunc3(lerp)
sfunc3(clamp)
sfunc1(smoothstep)
sfunc1(smootherstep)

static int slerp(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q0, q1;
    float t;
    if (nArgs != 3 || !variable_to_quaternion(args, q0) || !variable_to_quaternion(args + 1, q1) || !variable_to_float(args + 2, &t)) {
        fprintf(stderr, "Error: invalid argument for slerp, slerp(Quaternion, Quaternion, float)\n");
        return 0;
    }
    if (!sin(acos(dot4(q0, q1)))) {
        fprintf(stderr, "Error: slerp undefined when angle is 0 mod pi\n");
        return 0;
    }
    ret->type = QUATERNION;
    quaternion_slerp(ret->data.dquaternion, q0, q1, t);
    ret->type = QUATERNION;
    return 1;
}

int shell_make_math_functions(struct DemoShell* shell) {
    return builtin_function(shell, "vec2" , vec2_)
        && builtin_function(shell, "vec3" , vec3_)
        && builtin_function(shell, "vec4" , vec4_)
        && builtin_function(shell, "mat2" , mat2_)
        && builtin_function(shell, "mat3" , mat3_)
        && builtin_function(shell, "mat4" , mat4_)
        && builtin_function(shell, "quaternion" , quaternion)
        && builtin_function(shell, "axis" , axis)
        && builtin_function(shell, "angle" , angle)
        && builtin_function(shell, "rot3" , rot3)
        && builtin_function(shell, "rot4" , rot4)
        && builtin_function(shell, "rotation" , rotation)
        && builtin_function(shell, "swing" , swing)
        && builtin_function(shell, "twist" , twist)
        && builtin_function(shell, "normalize" , normalize)
        && builtin_function(shell, "norm" , norm)
        && builtin_function(shell, "tr" , transpose)
        && builtin_function(shell, "trace" , trace)
        && builtin_function(shell, "det" , det)
        && builtin_function(shell, "inv" , inv)
        && builtin_function(shell, "dot" , dot)
        && builtin_function(shell, "cross" , cross)
        && builtin_function(shell, "abs" , fabs_)
        && builtin_function(shell, "sqrt" , sqrt_)
        && builtin_function(shell, "pow" , pow_)
        && builtin_function(shell, "exp" , exp_)
        && builtin_function(shell, "log" , log_)
        && builtin_function(shell, "sin" , sin_)
        && builtin_function(shell, "cos" , cos_)
        && builtin_function(shell, "tan" , tan_)
        && builtin_function(shell, "asin" , asin_)
        && builtin_function(shell, "acos" , acos_)
        && builtin_function(shell, "atan" , atan_)
        && builtin_function(shell, "atan2" , atan2_)
        && builtin_function(shell, "lerp" , lerp_)
        && builtin_function(shell, "clamp" , clamp_)
        && builtin_function(shell, "smoothstep" , smoothstep_)
        && builtin_function(shell, "smootherstep" , smootherstep_)
        && builtin_function(shell, "slerp" , slerp);
}
