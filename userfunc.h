#include "parser.h"
#include "variable.h"
#include "eval.h"

#ifndef TDSH_USERFUNC_H
#define TDSH_USERFUNC_H

struct UserFunc {
    struct EvalState* evalState;
    struct DemoShell* shell;
    struct AstNode* body;
    char** argnames;
    unsigned int nArgs;
};

int userfunc_make(struct DemoShell* shell, struct AstNode* decl, struct Variable* dest);

#endif
