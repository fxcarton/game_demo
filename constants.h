#include "shell.h"

#ifndef TDSH_CONSTANTS_H
#define TDSH_CONSTANTS_H

int shell_make_constants(struct DemoShell* shell);

#endif
