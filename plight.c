#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/render/lights_buffer_object.h>
#include "builtins.h"
#include "lights.h"

struct PlightData {
    struct PointLight l;
    unsigned int i;
};

#define flt_ptr_get(a, b) float_ptr_get(&(a), b)
#define flt_ptr_set(a, b) float_ptr_set(a, &(b))

static int plight_ptr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.sdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}
static void plight_ptr_free(struct Variable* var) {
    shell_decref(var->data.dpointer.sdata);
}

#define PLIGHT_MEMBER(type, name) \
static int plight_##name##_get(const void* src, struct Variable* dest) { \
    const struct ShellRefCount* r = src; \
    struct PlightData* ld = r->data; \
    return type##_ptr_get(ld->l.name, dest); \
} \
static int plight_##name##_set(const struct Variable* src, void* dest) { \
    struct ShellRefCount* r = dest; \
    struct PlightData* ld = r->data; \
    if (!type##_ptr_set(src, ld->l.name)) return 0; \
    if (ld->i < MAX_POINT_LIGHTS) { \
        lights_buffer_object_update_plight(&r->shell->scene.lights, &ld->l, ld->i); \
        r->shell->lightsChanged = 1; \
    } \
    return 1; \
} \
static struct PointerInfo plight##_##name = {plight_##name##_get, plight_##name##_set, plight_ptr_copy, plight_ptr_free}; \
static int plight_ptr_##name(const void* src, struct Variable* dest) { \
    struct ShellRefCount* r = (void*)src; \
    if (!shell_incref(r)) return 0; \
    variable_make_ptr(&plight##_##name, r, r, dest); \
    return 1; \
}

PLIGHT_MEMBER(vec3, color)
PLIGHT_MEMBER(vec3, position)
PLIGHT_MEMBER(flt, radius)

static int plight_id_get(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct PlightData* ld = r->data;
    variable_make_int((ld->i < MAX_POINT_LIGHTS) ? (long)ld->i : -1L, dest);
    return 1;
}
static int plight_id_set(const struct Variable* src, void* dest) {
    struct ShellRefCount *r = dest, *old = NULL;
    struct PlightData* ld = r->data;
    long id;
    unsigned int i;
    if (!variable_to_int(src, &id)) return 0;
    if (id < 0) {
        if (ld->i >= MAX_POINT_LIGHTS) return 0;
        if ((old = r->shell->rplight[ld->i])) {
            if (--r->shell->numPointLights > ld->i) {
                struct PlightData* last = r->shell->rplight[r->shell->numPointLights]->data;
                r->shell->rplight[ld->i] = r->shell->rplight[r->shell->numPointLights];
                r->shell->rplight[r->shell->numPointLights] = NULL;
                last->i = ld->i;
                lights_buffer_object_update_plight(&r->shell->scene.lights, &last->l, last->i);
            }
            ld->i = -1;
            lights_buffer_object_update_nplight(&r->shell->scene.lights, r->shell->numPointLights);
            r->shell->lightsChanged = 1;
        }
    } else {
        if (id > MAX_POINT_LIGHTS) return 0;
        i = id;
        if (i >= r->shell->numPointLights) return 0;
        if (ld->i >= MAX_POINT_LIGHTS) {
            if (!shell_incref(r)) return 0;
        }
        if (ld->i < MAX_POINT_LIGHTS) {
            struct PlightData* other = r->shell->rplight[i]->data;
            r->shell->rplight[ld->i] = r->shell->rplight[i];
            other->i = ld->i;
            lights_buffer_object_update_plight(&r->shell->scene.lights, &other->l, other->i);
        } else {
            old = r->shell->rplight[i];
        }
        r->shell->rplight[i] = r;
        ld->i = i;
        lights_buffer_object_update_plight(&r->shell->scene.lights, &ld->l, ld->i);
        r->shell->lightsChanged = 1;
    }
    if (old) shell_decref(old);
    return 1;
}
static struct PointerInfo plight_id = {plight_id_get, plight_id_set, plight_ptr_copy, plight_ptr_free};
static int plight_ptr_id(const void* src, struct Variable* dest) {
    struct ShellRefCount* r = (void*)src;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(&plight_id, r, r, dest);
    return 1;
}

static const struct FieldInfo structPlightFields[] = {
    {"color", plight_ptr_color},
    {"position", plight_ptr_position},
    {"radius", plight_ptr_radius},
    {"id", plight_ptr_id},
    {0}
};

static const struct StructInfo structPlight;

static int plight_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dstruct.data)) return 0;
    variable_make_struct(&structPlight, src->data.dstruct.data, dest);
    return 1;
}

static void plight_free(void* data) {
    shell_decref(data);
}

static const struct StructInfo structPlight = {"PLight", structPlightFields, plight_copy, plight_free};

int variable_is_plight(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structPlight;
}

struct PointLight* variable_to_plight(const struct Variable* var) {
    if (variable_is_plight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct PlightData* ld = r->data;
        return &ld->l;
    }
    return NULL;
}

int variable_set_plight_index(const struct Variable* var, unsigned int i) {
    if (variable_is_plight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct PlightData* ld = r->data;
        ld->i = i;
        return 1;
    }
    return 0;
}

static int plight_add(void* dest, const struct Variable* src) {
    struct Variable tmp;
    struct DemoShell* shell = dest;
    struct ShellRefCount* r;
    int ok;
    if (shell->numPointLights >= MAX_POINT_LIGHTS
     || !variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if ((ok = variable_is_plight(&tmp) && ((struct PlightData*)(r = tmp.data.dstruct.data)->data)->i == ((unsigned int)-1))) {
        if (shell->rplight[shell->numPointLights]) {
            struct PlightData* ld = shell->rplight[shell->numPointLights]->data;
            ld->i = -1;
            shell_decref(shell->rplight[shell->numPointLights]);
        }
        shell->rplight[shell->numPointLights] = r;
        variable_set_plight_index(&tmp, shell->numPointLights);
        lights_buffer_object_update_plight(&shell->scene.lights, variable_to_plight(&tmp), shell->numPointLights);
        lights_buffer_object_update_nplight(&shell->scene.lights, ++shell->numPointLights);
        shell->lightsChanged = 1;
    } else {
        variable_free(&tmp);
    }
    return ok;
}

static int plight_del(void* dest, unsigned int i) {
    struct DemoShell* shell = dest;

    if (i >= shell->numPointLights) return 0;
    --(shell->numPointLights);
    if (shell->rplight[i]) {
        struct PlightData* ld = shell->rplight[i]->data;
        ld->i = -1;
        shell_decref(shell->rplight[i]);
        shell->rplight[i] = NULL;
    }
    if (shell->numPointLights) {
        shell->rplight[i] = shell->rplight[shell->numPointLights];
        shell->rplight[shell->numPointLights] = NULL;
        if (shell->rplight[i]) {
            struct PlightData* ld = shell->rplight[i]->data;
            ld->i = i;
            lights_buffer_object_update_plight(&shell->scene.lights, &ld->l, i);
        }
    }
    lights_buffer_object_update_nplight(&shell->scene.lights, shell->numPointLights);
    shell->lightsChanged = 1;
    return 1;
}

static int plight_get(const void* src, unsigned int i, struct Variable* dest) {
    const struct DemoShell* shell = src;
    struct ShellRefCount* r;

    if (i >= shell->numPointLights || !(r = shell->rplight[i]) || !shell_incref(r)) {
        return 0;
    }
    variable_make_struct(&structPlight, r, dest);
    return 1;
}

static void plight_destroy(struct DemoShell* shell, void* l) {
    free(l);
}

static int plight(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct PlightData* ld;
    struct PointLight* l;
    struct ShellRefCount* r;

    if (nArgs != 3) {
        fprintf(stderr, "Error: plight() needs 3 (vec3, vec3, float) arguments\n");
        return 0;
    }
    if (!(ld = malloc(sizeof(*ld)))) {
        fprintf(stderr, "Error: failed to allocate plight\n");
        return 0;
    }
    l = &ld->l;
    if (!variable_to_vec3(args, l->color) || !variable_to_vec3(args + 1, l->position) || !variable_to_float(args + 2, &l->radius)) {
        fprintf(stderr, "Error: plight() needs 3 (vec3, vec3, float) arguments\n");
        free(ld);
        return 0;
    }
    if (l->radius < 0.0) l->radius = 0.0;
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, plight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structPlight, r, ret);
    return 1;
}

static struct ArrayInfo arrayPlights = {plight_get, plight_add, plight_del, NULL, NULL};

int shell_make_plights(struct DemoShell* shell) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, "plights"))) {
        return 0;
    }
    variable_make_array(&arrayPlights, shell, &shell->numPointLights, dest);

    return builtin_function_with_data(shell, "plight", plight, shell);
}

int variable_make_plight(struct DemoShell* shell, const struct PointLight* pl, struct Variable* dest) {
    struct PlightData* ld;
    struct PointLight* l;
    struct ShellRefCount* r;

    if (!(ld = malloc(sizeof(*ld)))) {
        return 0;
    }
    l = &ld->l;
    *l = *pl;
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, plight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structPlight, r, dest);
    return 1;
}
