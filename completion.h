#include "shell.h"

#ifndef TDSH_COMPLETION_H
#define TDSH_COMPLETION_H

void shell_init_completion(struct DemoShell* s);

#endif
