#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/render/lights_buffer_object.h>
#include "builtins.h"
#include "lights.h"

struct SlightData {
    struct SpotLight l;
    unsigned int i;
};

#define vec3n_ptr_get(a, b) vec3_ptr_get(a, b)
#define vec3n_ptr_set(a, b) vec3_ptr_set_normalize(a, b)
#define flt_ptr_get(a, b) float_ptr_get(&(a), b)
#define flt_ptr_set(a, b) float_ptr_set(a, &(b))

static int slight_ptr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.sdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}
static void slight_ptr_free(struct Variable* var) {
    shell_decref(var->data.dpointer.sdata);
}

#define SLIGHT_MEMBER(type, name) \
static int slight_##name##_get(const void* src, struct Variable* dest) { \
    const struct ShellRefCount* r = src; \
    struct SlightData* ld = r->data; \
    return type##_ptr_get(ld->l.name, dest); \
} \
static int slight_##name##_set(const struct Variable* src, void* dest) { \
    struct ShellRefCount* r = dest; \
    struct SlightData* ld = r->data; \
    if (!type##_ptr_set(src, ld->l.name)) return 0; \
    if (ld->i < MAX_SPOT_LIGHTS) { \
        lights_buffer_object_update_slight(&r->shell->scene.lights, &ld->l, ld->i); \
        r->shell->lightsChanged = 1; \
    } \
    return 1; \
} \
static struct PointerInfo slight##_##name = {slight_##name##_get, slight_##name##_set, slight_ptr_copy, slight_ptr_free}; \
static int slight_ptr_##name(const void* src, struct Variable* dest) { \
    struct ShellRefCount* r = (void*)src; \
    if (!shell_incref(r)) return 0; \
    variable_make_ptr(&slight##_##name, r, r, dest); \
    return 1; \
}

SLIGHT_MEMBER(vec3, color)
SLIGHT_MEMBER(vec3, position)
SLIGHT_MEMBER(vec3n, direction)
SLIGHT_MEMBER(flt, intensity)
SLIGHT_MEMBER(flt, outerAngle)
SLIGHT_MEMBER(flt, innerAngle)

static int slight_id_get(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct SlightData* ld = r->data;
    variable_make_int((ld->i < MAX_SPOT_LIGHTS) ? (long)ld->i : -1L, dest);
    return 1;
}
static int slight_id_set(const struct Variable* src, void* dest) {
    struct ShellRefCount *r = dest, *old = NULL;
    struct SlightData* ld = r->data;
    long id;
    unsigned int i;
    if (!variable_to_int(src, &id)) return 0;
    if (id < 0) {
        if (ld->i >= MAX_SPOT_LIGHTS) return 0;
        if ((old = r->shell->rslight[ld->i])) {
            if (--r->shell->numSpotLights > ld->i) {
                struct SlightData* last = r->shell->rslight[r->shell->numSpotLights]->data;
                r->shell->rslight[ld->i] = r->shell->rslight[r->shell->numSpotLights];
                r->shell->rslight[r->shell->numSpotLights] = NULL;
                last->i = ld->i;
                lights_buffer_object_update_slight(&r->shell->scene.lights, &last->l, last->i);
            }
            ld->i = -1;
            lights_buffer_object_update_nslight(&r->shell->scene.lights, r->shell->numSpotLights);
            r->shell->lightsChanged = 1;
        }
    } else {
        if (id > MAX_SPOT_LIGHTS) return 0;
        i = id;
        if (i >= r->shell->numSpotLights) return 0;
        if (ld->i >= MAX_SPOT_LIGHTS) {
            if (!shell_incref(r)) return 0;
        }
        if (ld->i < MAX_SPOT_LIGHTS) {
            struct SlightData* other = r->shell->rslight[i]->data;
            r->shell->rslight[ld->i] = r->shell->rslight[i];
            other->i = ld->i;
            lights_buffer_object_update_slight(&r->shell->scene.lights, &other->l, other->i);
        } else {
            old = r->shell->rslight[i];
        }
        r->shell->rslight[i] = r;
        ld->i = i;
        lights_buffer_object_update_slight(&r->shell->scene.lights, &ld->l, ld->i);
        r->shell->lightsChanged = 1;
    }
    if (old) shell_decref(old);
    return 1;
}
static struct PointerInfo slight_id = {slight_id_get, slight_id_set, slight_ptr_copy, slight_ptr_free};
static int slight_ptr_id(const void* src, struct Variable* dest) {
    struct ShellRefCount* r = (void*)src;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(&slight_id, r, r, dest);
    return 1;
}

static const struct FieldInfo structSlightFields[] = {
    {"color", slight_ptr_color},
    {"position", slight_ptr_position},
    {"direction", slight_ptr_direction},
    {"intensity", slight_ptr_intensity},
    {"outerAngle", slight_ptr_outerAngle},
    {"innerAngle", slight_ptr_innerAngle},
    {"id", slight_ptr_id},
    {0}
};

static const struct StructInfo structSlight;

static int slight_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dstruct.data)) return 0;
    variable_make_struct(&structSlight, src->data.dstruct.data, dest);
    return 1;
}

static void slight_free(void* data) {
    shell_decref(data);
}

static const struct StructInfo structSlight = {"SLight", structSlightFields, slight_copy, slight_free};

int variable_is_slight(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structSlight;
}

struct SpotLight* variable_to_slight(const struct Variable* var) {
    if (variable_is_slight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct SlightData* ld = r->data;
        return &ld->l;
    }
    return NULL;
}

int variable_set_slight_index(const struct Variable* var, unsigned int i) {
    if (variable_is_slight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct SlightData* ld = r->data;
        ld->i = i;
        return 1;
    }
    return 0;
}

static int slight_add(void* dest, const struct Variable* src) {
    struct Variable tmp;
    struct DemoShell* shell = dest;
    struct ShellRefCount* r;
    int ok;
    if (shell->numSpotLights >= MAX_SPOT_LIGHTS
     || !variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if ((ok = variable_is_slight(&tmp) && ((struct SlightData*)(r = tmp.data.dstruct.data)->data)->i == ((unsigned int)-1))) {
        if (shell->rslight[shell->numSpotLights]) {
            struct SlightData* ld = shell->rslight[shell->numSpotLights]->data;
            ld->i = -1;
            shell_decref(shell->rslight[shell->numSpotLights]);
        }
        shell->rslight[shell->numSpotLights] = r;
        variable_set_slight_index(&tmp, shell->numSpotLights);
        lights_buffer_object_update_slight(&shell->scene.lights, variable_to_slight(&tmp), shell->numSpotLights);
        lights_buffer_object_update_nslight(&shell->scene.lights, ++shell->numSpotLights);
        shell->lightsChanged = 1;
    } else {
        variable_free(&tmp);
    }
    return ok;
}

static int slight_del(void* dest, unsigned int i) {
    struct DemoShell* shell = dest;

    if (i >= shell->numSpotLights) return 0;
    --(shell->numSpotLights);
    if (shell->rslight[i]) {
        struct SlightData* ld = shell->rslight[i]->data;
        ld->i = -1;
        shell_decref(shell->rslight[i]);
        shell->rslight[i] = NULL;
    }
    if (shell->numSpotLights) {
        shell->rslight[i] = shell->rslight[shell->numSpotLights];
        shell->rslight[shell->numSpotLights] = NULL;
        if (shell->rslight[i]) {
            struct SlightData* ld = shell->rslight[i]->data;
            ld->i = i;
            lights_buffer_object_update_slight(&shell->scene.lights, &ld->l, i);
        }
    }
    lights_buffer_object_update_nslight(&shell->scene.lights, shell->numSpotLights);
    shell->lightsChanged = 1;
    return 1;
}

static int slight_get(const void* src, unsigned int i, struct Variable* dest) {
    const struct DemoShell* shell = src;
    struct ShellRefCount* r;

    if (i >= shell->numSpotLights || !(r = shell->rslight[i]) || !shell_incref(r)) {
        return 0;
    }
    variable_make_struct(&structSlight, r, dest);
    return 1;
}

static void slight_destroy(struct DemoShell* shell, void* l) {
    free(l);
}

static int slight(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct SlightData* ld;
    struct SpotLight* l;
    struct ShellRefCount* r;

    if (nArgs != 6) {
        fprintf(stderr, "Error: slight() needs 6 (vec3, vec3, vec3, float, float, float) arguments\n");
        return 0;
    }
    if (!(ld = malloc(sizeof(*ld)))) {
        fprintf(stderr, "Error: failed to allocate slight\n");
        return 0;
    }
    l = &ld->l;
    if (!variable_to_vec3(args, l->color) || !variable_to_vec3(args + 1, l->position) || !variable_to_vec3(args + 2, l->direction)
     || !variable_to_float(args + 3, &l->intensity) || !variable_to_float(args + 4, &l->outerAngle) || !variable_to_float(args + 5, &l->innerAngle)) {
        fprintf(stderr, "Error: slight() needs 6 (vec3, vec3, vec3, float, float, float) arguments\n");
        free(ld);
        return 0;
    }
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, slight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structSlight, r, ret);
    return 1;
}

static struct ArrayInfo arraySlights = {slight_get, slight_add, slight_del, NULL, NULL};

int shell_make_slights(struct DemoShell* shell) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, "slights"))) {
        return 0;
    }
    variable_make_array(&arraySlights, shell, &shell->numSpotLights, dest);

    return builtin_function_with_data(shell, "slight", slight, shell);
}

int variable_make_slight(struct DemoShell* shell, const struct SpotLight* sl, struct Variable* dest) {
    struct SlightData* ld;
    struct SpotLight* l;
    struct ShellRefCount* r;

    if (!(ld = malloc(sizeof(*ld)))) {
        return 0;
    }
    l = &ld->l;
    *l = *sl;
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, slight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structSlight, r, dest);
    return 1;
}
