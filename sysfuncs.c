#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include <errno.h>
#include <3dmr/render/viewer.h>
#include "builtins.h"
#include "shell.h"
#include "help.h"
#ifdef _POSIX_C_SOURCE
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>
#endif

static int print(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    while (nArgs) {
        variable_print(args++, 0, 0);
        nArgs--;
    }
    fputc('\n', stdout);
    ret->type = VOID;
    return 1;
}

static int tmpprint(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    fputs("\x1B[2K\r", stdout);
    while (nArgs) {
        variable_print(args++, 0, 0);
        nArgs--;
    }
    fflush(stdout);
    ret->type = VOID;
    return 1;
}

static int exit_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    if (!pthread_mutex_lock(&shell->mainMutex)) {
        shell->running = 0;
        pthread_mutex_unlock(&shell->mainMutex);
    } else {
        fprintf(stderr, "Error: in exit(), failed to lock shell mutex\n");
    }
    return 0;
}

static int error_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    printf("Error: ");
    if (nArgs) {
        print(data, args, nArgs, ret);
    } else {
        printf("error() function called\n");
    }
    return 0;
}

static int system_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    char* cmd;
    if (nArgs != 1 || !(cmd = variable_to_string(args))) {
        fprintf(stderr, "Error: system(): expected 1 string argument\n");
        return 0;
    }
    variable_make_int(system(cmd), ret);
    free(cmd);
    return 1;
}

#ifdef _POSIX_C_SOURCE
static int exec_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    char** argv;
    unsigned int i;
    pid_t pid;
    int ok = 0;

    if (!nArgs) {
        fprintf(stderr, "Error: system(): expected command line\n");
        return 0;
    }
    if (!(argv = malloc(nArgs * sizeof(*argv)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        return 0;
    }
    for (i = 0; i < nArgs; i++) {
        if (!(argv[i] = variable_to_string(args + i))) {
            while (i) free(argv[--i]);
            free(argv);
            fprintf(stderr, "Error: system(): expected string\n");
            return 0;
        }
    }
    if ((pid = fork()) == -1) {
        fprintf(stderr, "Error: system(): expected string\n");
    } else if (!pid) {
        execvp(*argv, argv);
        fprintf(stderr, "Error: execvp returned\n");
        exit(1);
    } else {
        int status;
        if (waitpid(pid, &status, 0) == pid) {
            if (WIFEXITED(status)) {
                ok = 1;
                variable_make_int(WEXITSTATUS(status), ret);
            } else if (WIFSIGNALED(status)) {
                fprintf(stderr, "Error: process caught signal %d\n", (int)(WTERMSIG(status)));
            } else {
                fprintf(stderr, "Error: waitpid failed\n");
            }
        } else {
            fprintf(stderr, "Error: waitpid failed\n");
        }
    }
    for (i = 0; i < nArgs; i++) {
        free(argv[i]);
    }
    free(argv);
    return ok;
}
#endif

static int title_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    char* t;
    if (nArgs != 1 || !(t = variable_to_string(args))) {
        fprintf(stderr, "Error: title: expected string argument\n");
        return 0;
    }
    viewer_set_title(shell->viewer, t);
    free(t);
    ret->type = VOID;
    return 1;
}

static int sprintf_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    const char* p;
    char *s = NULL, *format, *d, *t;
    size_t n;
    int ok = 0, k;

    if (!nArgs || !(format = variable_to_string(args))) {
        fprintf(stderr, "Error: sprintf: expected format string\n");
        return 0;
    }
    if ((n = strlen(format)) == ((size_t)-1) || !(d = s = malloc(n + 1))) {
        fprintf(stderr, "Error: sprintf: memory allocation failed\n");
    } else {
        ok = 1;
        for (p = format; ok && *p; p++) {
            if (*p != '%') {
                *d++ = *p;
            } else switch (*++p) {
                case 'd':
                    {
                        long v;
                        if (!(ok = (--nArgs) && variable_to_int(++args, &v))) {
                            fprintf(stderr, "Error: sprintf: %%d expected int variable\n");
                        } else if (!(ok = (n < ((size_t)-20) && (t = realloc(s, n + 20))))) {
                            fprintf(stderr, "Error: sprintf: memory allocation failed\n");
                        } else {
                            d = t + (d - s);
                            s = t;
                            n += 20;
                            if (!(ok = ((k = sprintf(d, "%ld", v)) > 0))) {
                                fprintf(stderr, "Error: sprintf: int conversion failed\n");
                            } else {
                                d += k;
                            }
                        }
                    }
                    break;
                case 'f':
                case 'g':
                    {
                        char fmt[3];
                        float v;
                        fmt[0] = '%';
                        fmt[1] = *p;
                        fmt[2] = 0;
                        k = 3 + DBL_MANT_DIG - DBL_MIN_EXP;
                        if (!(ok = (--nArgs) && variable_to_float(++args, &v))) {
                            fprintf(stderr, "Error: sprintf: %%f expected float variable\n");
                        } else if (!(ok = (n <= (((size_t)-1) - ((size_t)k)) && (t = realloc(s, n + k))))) {
                            fprintf(stderr, "Error: sprintf: memory allocation failed\n");
                        } else {
                            d = t + (d - s);
                            s = t;
                            n += k;
                            if (!(ok = ((k = sprintf(d, fmt, v)) > 0))) {
                                fprintf(stderr, "Error: sprintf: float conversion failed\n");
                            } else {
                                d += k;
                            }
                        }
                    }
                    break;
                case 's':
                    {
                        size_t k;
                        char* a;
                        if (!(ok = (--nArgs) && (a = variable_to_string(++args)))) {
                            fprintf(stderr, "Error: sprintf: %%s expected string variable\n");
                        } else if (!(ok = (n <= (((size_t)-1) - (k = strlen(a))) && (t = realloc(s, n + k))))) {
                            fprintf(stderr, "Error: sprintf: memory allocation failed\n");
                        } else {
                            d = t + (d - s);
                            s = t;
                            n += k;
                            memcpy(d, a, k);
                            d += k;
                        }
                    }
                    break;
                default:
                    fprintf(stderr, "Error: sprintf: invalid format character '%c'\n", *p);
                    ok = 0;
            }
        }
    }
    free(format);
    if (ok) {
        *d = 0;
        ret->type = STRING;
        ret->data.dstring = s;
    } else {
        free(s);
    }
    return ok;
}

#define ENABLE(name) \
static int enable_##name(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) { \
    struct DemoShell* shell = data; \
    long v; \
    if (nArgs != 1 || !variable_to_int(args, &v)) { \
        fprintf(stderr, "Error: enable_"#name": expected int argument\n"); \
        return 0; \
    } \
    if (v < 0) { \
        shell->enable##name = !shell->enable##name; \
    } else { \
        shell->enable##name = !!v; \
    } \
    ret->type = VOID; \
    return 1; \
}
ENABLE(Render)
ENABLE(Exec)

static int add(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable array;
    int ptr, ok = 0;
    if (nArgs != 2) {
        fprintf(stderr, "Error: add() takes two arguments\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &array);
    if (array.type != ARRAY) {
        fprintf(stderr, "Error: add(): not an array\n");
    } else if (!variable_array_add(&array, args + 1)) {
        fprintf(stderr, "Error: add(): failed to add element (check type/array max size)\n");
    } else {
        ok = 1;
        variable_make_int(*array.data.darray.count - 1, ret);
    }
    if (ptr) variable_free(&array);
    return ok;
}

static int del(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable array;
    long index;
    int ptr, ok = 0;
    if (nArgs != 2) {
        fprintf(stderr, "Error: del() takes two arguments\n");
        return 0;
    }
    if (!variable_to_int(args + 1, &index)) {
        fprintf(stderr, "Error: del(): invalid index\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &array);
    if (array.type != ARRAY) {
        fprintf(stderr, "Error: del(): not an array\n");
    } else if (!variable_array_del(&array, index)) {
        fprintf(stderr, "Error: del(): failed to delete element (check bounds)\n");
    } else {
        ok = 1;
        ret->type = VOID;
    }
    if (ptr) variable_free(&array);
    return ok;
}

static int len(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable array;
    int ptr, ok = 0;
    if (nArgs != 1) {
        fprintf(stderr, "Error: len() takes one array argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &array);
    if (array.type != ARRAY) {
        fprintf(stderr, "Error: len(): not an array\n");
    } else {
        ok = 1;
        variable_make_int(*array.data.darray.count, ret);
    }
    if (ptr) variable_free(&array);
    return ok;
}

static int wait_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
        float t;
        if (nArgs != 1 || !variable_to_float(args, &t)) {
            fprintf(stderr, "Error: wait() takes one float argument\n");
            return 0;
        }
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 199309L
        {
            struct timespec ts;
            ts.tv_sec = t;
            ts.tv_nsec = fmod(t, 1.0) * 1e9;
            while (nanosleep(&ts, &ts) && errno == EINTR);
        }
        ret->type = VOID;
        return 1;
#else
        fprintf(stderr, "Error: wait(): not implemented\n");
        return 0;
#endif
}

static int chdir_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    char* dir;
    int err;

    if (nArgs == 0) {
        const char *start, *cur, *end;
        for (start = cur = end = ((struct DemoShell*)data)->curFile; *cur; cur++) {
            if (*cur == '/') end = cur;
        }
        if (!(dir = malloc(end - start + 1))) {
            fprintf(stderr, "Error: memory allocation failed\n");
            return 0;
        }
        memcpy(dir, start, end - start);
        dir[end - start] = 0;
    } else if (nArgs == 1) {
        if (!(dir = variable_to_string(args))) {
            fprintf(stderr, "Error: chdir(): expected string\n");
            return 0;
        }
    } else {
        fprintf(stderr, "Error: chdir(): expected 0 or 1 argument\n");
        return 0;
    }
    if ((err = chdir(dir))) {
        fprintf(stderr, "Error: chdir(): failed to change directory to '%s'\n", dir);
    }
    free(dir);
    ret->type = VOID;
    return !err;
}

static int help_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    const struct Help* h = help;
    char* arg;
    ret->type = VOID;
    if (!nArgs) {
        fputs("Help topics:\n", stdout);
        while (h->name) {
            printf("- %s\n", h->name);
            h++;
        }
        return 1;
    }
    if (nArgs != 1 || !(arg = variable_to_string(args))) {
        fprintf(stderr, "Error: help takes one string argument\n");
        return 0;
    }
    while (h->name) {
        if (!strcmp(h->name, arg)) {
            fputs(h->doc, stdout);
            fputc('\n', stdout);
            break;
        }
        h++;
    }
    if (!h->name) {
        printf("help: %s: not found\n", arg);
    }
    free(arg);
    return 1;
}

#ifdef __linux__
#include <dlfcn.h>
#define PLUGIN_DLOPEN 1
#endif

static int (*plugin_get_init(void* plugin))(struct DemoShell*, unsigned int) {
    int (*init)(struct DemoShell*, unsigned int);
    *(void**)(&init) = dlsym(plugin, "demo_plugin_init");
    return init;
}

static int plugin_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
#ifdef PLUGIN_DLOPEN
    const char* pluginPaths;
    char *name, *path;
    void* plugin = NULL;
    int (*init)(struct DemoShell*, unsigned int);
    struct DemoShell* shell = data;
    size_t n, m;
    int ok = 0;
    while (nArgs--) {
        if (!(name = variable_to_string(args++))) {
            fprintf(stderr, "Error: expected plugin name (string)\n");
            return 0;
        }
        n = strlen(name);
        if ((pluginPaths = getenv("DEMO_PLUGIN_PATH"))) {
            const char* end;
            do {
                for (end = pluginPaths; *end && *end != ':'; end++);
                m = end - pluginPaths;
                if (m >= ((size_t)-2) || (m + 2U) > ((size_t)-1) - n
                 || !(path = malloc(n + (m + 2U)))) {
                    fprintf(stderr, "Error: failed to allocate memory for path\n");
                    free(name);
                    return 0;
                }
                memcpy(path, pluginPaths, m);
                path[m++] = '/';
                memcpy(path + m, name, n + 1U);
                plugin = dlopen(path, RTLD_LOCAL | RTLD_NOW | RTLD_NODELETE);
                free(path);
                pluginPaths += m;
            } while (!plugin && *end);
        }
        if (!plugin) {
            m = strlen(DEMO_PLUGIN_PATH);
            if (m >= ((size_t)-2) || (m + 2U) > ((size_t)-1) - n
             || !(path = malloc(n + (m + 2U)))) {
                fprintf(stderr, "Error: failed to allocate memory for path\n");
                free(name);
                return 0;
            }
            memcpy(path, DEMO_PLUGIN_PATH, m);
            path[m++] = '/';
            memcpy(path + m, name, n + 1U);
            plugin = dlopen(path, RTLD_LOCAL | RTLD_NOW | RTLD_NODELETE);
            free(path);
        }
        if (!plugin) {
            fprintf(stderr, "Error: plugin '%s' not found\n", name);
        } else if (!(init = plugin_get_init(plugin))) {
            fprintf(stderr, "Error: invalid plugin '%s'\n", name);
        } else if (!init(shell, DEMO_API_VERSION)) {
            fprintf(stderr, "Error: failed to initialize plugin '%s'\n", name);
        } else {
            ok = 1;
        }
        free(name);
        if (plugin) dlclose(plugin);
    }
    ret->type = VOID;
    return ok;
#else
    fprintf(stderr, "Error: plugin not implemented on this platform\n");
    return 0;
#endif
}

static int lasterr_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    variable_make_int(shell->lasterr, ret);
    return 1;
}

int shell_make_sys_functions(struct DemoShell* shell) {
    return builtin_function(shell, "print", print)
        && builtin_function(shell, "tmpprint", tmpprint)
        && builtin_function_with_data(shell, "exit", exit_, shell)
        && builtin_function(shell, "error", error_)
        && builtin_function(shell, "system", system_)
#ifdef _POSIX_C_SOURCE
        && builtin_function(shell, "exec", exec_)
#endif
        && builtin_function_with_data(shell, "title", title_, shell)
        && builtin_function_with_data(shell, "sprintf", sprintf_, shell)
        && builtin_function_with_data(shell, "chdir", chdir_, shell)
        && builtin_function_with_data(shell, "enable_render", enable_Render, shell)
        && builtin_function_with_data(shell, "enable_exec", enable_Exec, shell)
        && builtin_function(shell, "add", add)
        && builtin_function(shell, "del", del)
        && builtin_function(shell, "len", len)
        && builtin_function(shell, "wait", wait_)
        && builtin_function(shell, "help", help_)
        && builtin_function_with_data(shell, "plugin", plugin_, shell)
        && builtin_function_with_data(shell, "lasterr", lasterr_, shell);
}
