#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <3dmr/math/random.h>
#include "builtins.h"
#include "random.h"

static int mtrand_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dstruct.data)) return 0;
    variable_make_struct(src->data.dstruct.info, src->data.dstruct.data, dest);
    return 1;
}

static void mtrand_free(void* d) {
    shell_decref(d);
}

static void mtrand_destroy(struct DemoShell* shell, void* d) {
    free(d);
}

static const struct FieldInfo structMTRandFields[] = {{0}};
static const struct StructInfo structMTRand = {"MTRand", structMTRandFields, mtrand_copy, mtrand_free};

static int mtrand(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    long seed;
    struct MTState* state;
    struct ShellRefCount* r;
    struct DemoShell* shell = data;

    if (nArgs != 1 || !variable_to_int(args, &seed)) {
        fprintf(stderr, "Error: mtrand: invalid seed\n");
        return 0;
    }
    if (!(state = mt_state()) || !(r = shell_new_refcount(shell, mtrand_destroy, state))) {
        free(state);
        fprintf(stderr, "Error: mtrand: failed to allocate state\n");
        return 0;
    }
    mt_seed(state, seed);
    variable_make_struct(&structMTRand, r, ret);
    return 1;
}

static int randseed_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    long seed;
    struct MTState* state;
    int ok = 0;
    if (!nArgs || !variable_to_int(args, &seed)) {
        fprintf(stderr, "Error: randseed: invalid seed\n");
    } else if (nArgs >= 2) {
        struct Variable tmp;
        int ptr = variable_move_dereference(args + 1, &tmp);
        if ((state = variable_to_mtstate(&tmp))) {
            mt_seed(state, seed);
            ok = 1;
        } else {
            fprintf(stderr, "Error: randseed: invalid MTRand\n");
        }
        if (ptr) variable_free(&tmp);
    } else {
        random_seed(seed);
        ok = 1;
    }
    ret->type = VOID;
    return ok;
}

static int randint_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct MTState* state;
    unsigned long val;
    if (nArgs) {
        struct Variable tmp;
        int ptr = variable_move_dereference(args, &tmp);
        if ((state = variable_to_mtstate(&tmp))) {
            val = mt_ulong_rand(state);
        } else {
            fprintf(stderr, "Error: randint: invalid MTRand\n");
            if (ptr) variable_free(&tmp);
            return 0;
        }
        if (ptr) variable_free(&tmp);
    } else {
        val = random_ulong();
    }
    variable_make_int(val & (((unsigned long)-1) >> 1), ret);
    return 1;
}

static int randrange_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    unsigned long size, cur, mask;
    long min, max;
    if (nArgs < 2 || !variable_to_int(args, &min) || !variable_to_int(args + 1, &max)
     || min > max || ((min < 0) ? (max > INT_MAX + min) : (max < INT_MIN + min))) {
        fprintf(stderr, "Error: randrange: invalid range\n");
        return 0;
    }
    mask = size = ((unsigned long)(max - min)) + 1UL;
    mask |= mask >> 1;
    mask |= mask >> 2;
    mask |= mask >> 4;
    mask |= mask >> 8;
    mask |= mask >> 16;
    mask |= mask >> 32;
    do {
        if (!randint_(data, args + 2, nArgs - 2, ret)) return 0;
        cur = ret->data.dint;
        cur &= mask;
    } while (cur >= size);
    variable_make_int(((long)cur) + min, ret);
    return 1;
}

static int randflt_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct MTState* state;
    float val;
    if (nArgs) {
        struct Variable tmp;
        int ptr = variable_move_dereference(args, &tmp);
        if ((state = variable_to_mtstate(&tmp))) {
            val = mt_float_rand(state);
        } else {
            fprintf(stderr, "Error: randflt: invalid MTRand\n");
            if (ptr) variable_free(&tmp);
            return 0;
        }
        if (ptr) variable_free(&tmp);
    } else {
        val = random_float();
    }
    variable_make_float(val, ret);
    return 1;
}

int shell_make_random_functions(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "mtrand", mtrand, shell)
        && builtin_function(shell, "randseed", randseed_)
        && builtin_function(shell, "randint", randint_)
        && builtin_function(shell, "randrange", randrange_)
        && builtin_function(shell, "randflt", randflt_);
}

int variable_is_mtstate(const struct Variable* v) {
    return v->type == STRUCT && v->data.dstruct.info == &structMTRand;
}

struct MTState* variable_to_mtstate(const struct Variable* v) {
    if (variable_is_mtstate(v)) {
        struct ShellRefCount* r = v->data.dstruct.data;
        return r->data;
    }
    return NULL;
}
