#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lexer.h"
#include "parser.h"
#include "operators.h"

enum Op {
    OP_UNARY_MINUS = 1,
    OP_UNARY_PLUS,
    OP_PREINCR,
    OP_PREDECR,
    OP_POSTINCR,
    OP_POSTDECR,
    OP_PLUSEQUALS,
    OP_MINUSEQUALS,
    OP_TIMESEQUALS,
    OP_DIVEQUALS,
    OP_MODEQUALS,
    OP_LESSEQUALS,
    OP_GREATEREQUALS,
    OP_EQUALS,
    OP_NOTEQUALS,
    OP_AND,
    OP_OR,
    OP_COLONEQUALS,
    OP_CONTROL_FLOW,
    OP_ELSE,
    OP_USER_FUNC
};

#define SET_PARSE_ERROR(parser, errormsg) {(parser)->error = errormsg; (parser)->status = PARSE_ERROR;}

static int push(void*** stack, unsigned int* count, void* val) {
    void** tmp;
    if ((tmp = realloc(*stack, (*count + 1) * sizeof(void*)))) {
        *stack = tmp;
        tmp += (*count)++;
        *tmp = val;
        return 1;
    }
    return 0;
}

static struct AstNode* push_node(struct Parser* parser) {
    struct AstNode* node;
    if (!(node = malloc(sizeof(*node))) || !push((void***)&parser->expressions, &parser->numExprs, node)) {
        free(node);
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return NULL;
    }
    return node;
}

static int push_op(struct Parser* parser, char* op) {
    if (!push((void***)&parser->operators, &parser->numOps, op)) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    return 1;
}

static int push_op_char(struct Parser* parser, char op) {
    char* o;
    if (!(o = malloc(1)) || !push((void***)&parser->operators, &parser->numOps, o)) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        free(o);
        return 0;
    }
    *o = op;
    return 1;
}

static char matching(char c) {
    switch (c) {
        case '(': return ')';
        case ')': return '(';
        case '[': return ']';
        case ']': return '[';
        case '{': return '}';
        case '}': return '{';
    }
    return 0;
}

static const char* unexpected(char c) {
    switch (c) {
        case ')': return "unexpected ')'";
        case ']': return "unexpected ']'";
        case '}': return "unexpected '}'";
    }
    return 0;
}

static const char* mismatched(char c) {
    switch (c) {
        case ')': return "mismatched ')'";
        case ']': return "mismatched ']'";
        case '}': return "mismatched '}'";
    }
    return 0;
}

static int precedence(char op) {
    switch (op) {
        case ';':
            return 0;
        case OP_CONTROL_FLOW: case OP_ELSE: case OP_USER_FUNC:
            return 1;
        case '=': case OP_COLONEQUALS:
        case OP_PLUSEQUALS: case OP_MINUSEQUALS:
        case OP_TIMESEQUALS: case OP_DIVEQUALS: case OP_MODEQUALS:
            return 2;
        case OP_OR:
            return 3;
        case OP_AND:
            return 4;
        case OP_EQUALS: case OP_NOTEQUALS:
            return 5;
        case '<': case '>':
        case OP_LESSEQUALS: case OP_GREATEREQUALS:
            return 6;
        case ',':
            return 7;
        case '+': case '-':
            return 8;
        case '*': case '/': case '%':
            return 9;
        case OP_PREINCR: case OP_PREDECR:
        case OP_UNARY_MINUS: case OP_UNARY_PLUS: case '!':
            return 10;
        case OP_POSTINCR: case OP_POSTDECR:
            return 11;
        case '.':
            return 12;
    }
    return -1;
}

static int left_associative(char op) {
    switch (op) {
        case '=': case OP_COLONEQUALS:
        case OP_PLUSEQUALS: case OP_MINUSEQUALS:
        case OP_TIMESEQUALS: case OP_DIVEQUALS: case OP_MODEQUALS:
        case OP_PREINCR: case OP_PREDECR:
        case OP_UNARY_MINUS: case OP_UNARY_PLUS: case '!':
        case OP_ELSE:
            return 0;
    }
    return 1;
}

static int binop(struct Parser* parser, int (*op)(struct Variable* a, struct Variable* b, struct Variable* dest, char** err), enum Op sym) {
    struct AstNode* node;
    if (parser->numExprs < 2) {
        SET_PARSE_ERROR(parser, "binary op without two expressions");
        return 0;
    }
    if (!(node = malloc(sizeof(*node)))) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    node->type = BINARY_OP;
    node->data.binaryOp.op = op;
    node->data.binaryOp.b = parser->expressions[--parser->numExprs];
    node->data.binaryOp.a = parser->expressions[--parser->numExprs];
    parser->expressions[parser->numExprs++] = node;
    if (op == variable_assign && sym == '=' && node->data.binaryOp.a->type == IDENTIFIER) {
        node->data.binaryOp.a->data.identifier.isDecl = 1;
    }
    return 1;
}

static int unop(struct Parser* parser, int (*op)(struct Variable* v, struct Variable* dest, char** err)) {
    struct AstNode* node;
    if (parser->numExprs < 1) {
        SET_PARSE_ERROR(parser, "unary op without expression");
        return 0;
    }
    if (!(node = malloc(sizeof(*node)))) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    node->type = UNARY_OP;
    node->data.unaryOp.op = op;
    node->data.unaryOp.v = parser->expressions[--parser->numExprs];
    parser->expressions[parser->numExprs++] = node;
    return 1;
}

static int structmbr(struct Parser* parser) {
    struct AstNode* node;
    if (parser->numExprs < 2 || parser->expressions[parser->numExprs - 1]->type != IDENTIFIER) {
        SET_PARSE_ERROR(parser, "invalid dot usage");
        return 0;
    }
    if (!(node = malloc(sizeof(*node)))) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    node->type = STRUCT_MEMBER;
    node->data.structMember.member = parser->expressions[--parser->numExprs];
    node->data.structMember.s = parser->expressions[--parser->numExprs];
    parser->expressions[parser->numExprs++] = node;
    return 1;
}

static int arraymbr(struct Parser* parser) {
    struct AstNode* node;
    if (parser->numExprs < 2) {
        SET_PARSE_ERROR(parser, "invalid [] usage");
        return 0;
    }
    if (!(node = malloc(sizeof(*node)))) {
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    node->type = ARRAY_ACCESS;
    node->data.arrayAccess.index = parser->expressions[--parser->numExprs];
    node->data.arrayAccess.array = parser->expressions[--parser->numExprs];
    parser->expressions[parser->numExprs++] = node;
    return 1;
}

static int funcall(struct Parser* parser, int hasArg) {
    struct AstNode* node;
    enum ControlFlowType controlFlow = 0;
    int isUserFunc = 0;

    if ((hasArg && !parser->numExprs) || !parser->numOps) {
        SET_PARSE_ERROR(parser, "invalid function call");
        return 0;
    }
    if (!strcmp(parser->operators[parser->numOps - 1], "if")) {
        controlFlow = IF;
    } else if (!strcmp(parser->operators[parser->numOps - 1], "for")) {
        controlFlow = FOR;
    } else if (!strcmp(parser->operators[parser->numOps - 1], "while")) {
        controlFlow = WHILE;
    } else if (!strcmp(parser->operators[parser->numOps - 1], "function")) {
        isUserFunc = 1;
    }
    if (controlFlow && !hasArg) {
        SET_PARSE_ERROR(parser, "missing condition");
        return 0;
    }
    if (controlFlow == FOR) {
        node = parser->expressions[parser->numExprs - 1];
        if (node->type != BLOC || !node->data.bloc.open || node->data.bloc.count != 3) {
            SET_PARSE_ERROR(parser, "invalid for condition, expected 3 semicolon-separated expr");
            return 0;
        }
    } else if (isUserFunc && hasArg) {
        unsigned int i;
        node = parser->expressions[parser->numExprs - 1];
        if (node->type != IDENTIFIER && (node->type != TUPLE || !node->data.tuple.open || !node->data.tuple.count)) {
            SET_PARSE_ERROR(parser, "invalid function argument list");
            return 0;
        }
        if (node->type != TUPLE) {
            for (i = 0; i < node->data.tuple.count; i++) {
                if (node->data.tuple.list[i]->type != IDENTIFIER) {
                    SET_PARSE_ERROR(parser, "invalid function argument identifier");
                    return 0;
                }
            }
        }
    }
    if (hasArg) {
        if (!(node = malloc(sizeof(*node)))) {
            SET_PARSE_ERROR(parser, "memory allocation failed");
            return 0;
        }
    } else {
        if (!(node = push_node(parser))) return 0;
    }
    if (controlFlow) {
        node->type = CONTROL_FLOW;
        node->data.controlFlow.type = controlFlow;
        node->data.controlFlow.condition = parser->expressions[--parser->numExprs];
        node->data.controlFlow.body = NULL;
        node->data.controlFlow.elseBody = NULL;
        parser->expressions[parser->numExprs++] = node;
        parser->operators[parser->numOps - 1][0] = OP_CONTROL_FLOW;
        parser->status = PARSE_OPERAND;
    } else if (isUserFunc) {
        node->type = FUNCTION_DECL;
        parser->operators[parser->numOps - 1][0] = OP_USER_FUNC;
        if (hasArg) {
            node->data.functionDecl.arglist = parser->expressions[--parser->numExprs];
            parser->expressions[parser->numExprs++] = node;
        } else {
            node->data.functionDecl.arglist = NULL;
        }
        node->data.functionDecl.body = NULL;
        parser->status = PARSE_OPERAND;
    } else {
        node->type = FUNCTION_CALL;
        node->data.functionCall.function = parser->operators[--parser->numOps];
        if (hasArg) {
            node->data.functionCall.args = parser->expressions[--parser->numExprs];
            parser->expressions[parser->numExprs++] = node;
        } else {
            node->data.functionCall.args = NULL;
        }
    }
    return 1;
}

static int comma(struct Parser* parser) {
    struct AstNode *tuple, **list;
    int existing;

    if (parser->numExprs < 2) {
        SET_PARSE_ERROR(parser, "invalid ',' usage");
        return 0;
    }
    if ((existing = (parser->expressions[parser->numExprs - 2]->type == TUPLE && parser->expressions[parser->numExprs - 2]->data.tuple.open))) {
        tuple = parser->expressions[parser->numExprs - 2];
        list = tuple->data.tuple.list;
    } else {
        if (!(tuple = malloc(sizeof(*tuple)))) {
            SET_PARSE_ERROR(parser, "memory allocation failed");
            return 0;
        }
        tuple->type = TUPLE;
        list = NULL;
        tuple->data.tuple.count = 1;
        tuple->data.tuple.open = 1;
    }
    if (!(tuple->data.tuple.list = realloc(list, (tuple->data.tuple.count + 1) * sizeof(*list)))) {
        if (!existing) free(tuple);
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    tuple->data.tuple.list[tuple->data.tuple.count++] = parser->expressions[parser->numExprs - 1];
    if (!existing) {
        tuple->data.tuple.list[0] = parser->expressions[parser->numExprs - 2];
        parser->expressions[parser->numExprs - 2] = tuple;
    }
    parser->numExprs--;
    return 1;
}

static int semicolon(struct Parser* parser) {
    struct AstNode *bloc, **list;
    int existing;

    if (parser->numExprs == 1 && parser->numOps == 1) {
        struct AstNode* node;
        if (!(node = push_node(parser))) return 0;
        node->type = CONSTANT;
        node->data.constant.value.type = VOID;
    } else if (parser->numExprs < 2) {
        SET_PARSE_ERROR(parser, "invalid ';' usage");
        return 0;
    }
    if ((existing = (parser->expressions[parser->numExprs - 2]->type == BLOC && parser->expressions[parser->numExprs - 2]->data.bloc.open))) {
        bloc = parser->expressions[parser->numExprs - 2];
        list = bloc->data.bloc.list;
    } else {
        if (!(bloc = malloc(sizeof(*bloc)))) {
            SET_PARSE_ERROR(parser, "memory allocation failed");
            return 0;
        }
        bloc->type = BLOC;
        list = NULL;
        bloc->data.bloc.count = 1;
        bloc->data.bloc.open = 1;
    }
    if (!(bloc->data.bloc.list = realloc(list, (bloc->data.bloc.count + 1) * sizeof(*list)))) {
        if (!existing) free(bloc);
        SET_PARSE_ERROR(parser, "memory allocation failed");
        return 0;
    }
    bloc->data.bloc.list[bloc->data.bloc.count++] = parser->expressions[parser->numExprs - 1];
    if (!existing) {
        bloc->data.bloc.list[0] = parser->expressions[parser->numExprs - 2];
        parser->expressions[parser->numExprs - 2] = bloc;
    }
    parser->numExprs--;
    return 1;
}

static int ctrlflowbody(struct Parser* parser) {
    struct AstNode* cf;
    if (parser->numExprs < 2 || (cf = parser->expressions[parser->numExprs - 2])->type != CONTROL_FLOW) {
        SET_PARSE_ERROR(parser, "invalid control flow construct");
        return 0;
    }
    cf->data.controlFlow.body = parser->expressions[--parser->numExprs];
    return 1;
}

static int elsebody(struct Parser* parser) {
    struct AstNode* cf;
    if (parser->numExprs < 2 || (cf = parser->expressions[parser->numExprs - 2])->type != CONTROL_FLOW || cf->data.controlFlow.type != IF) {
        SET_PARSE_ERROR(parser, "invalid else construct");
        return 0;
    }
    cf->data.controlFlow.elseBody = parser->expressions[--parser->numExprs];
    return 1;
}

static int userfunc(struct Parser* parser) {
    struct AstNode* f;
    if (parser->numExprs < 2 || (f = parser->expressions[parser->numExprs - 2])->type != FUNCTION_DECL || f->data.functionDecl.body) {
        SET_PARSE_ERROR(parser, "invalid user function construct");
        return 0;
    }
    f->data.functionDecl.body = parser->expressions[--parser->numExprs];
    return 1;
}

static int apply_op(struct Parser* parser, char op) {
    switch (op) {
        case '+': return binop(parser, variable_add, op);
        case '-': return binop(parser, variable_sub, op);
        case '*': return binop(parser, variable_mul, op);
        case '/': return binop(parser, variable_div, op);
        case '%': return binop(parser, variable_mod, op);
        case '<': return binop(parser, variable_lt, op);
        case '>': return binop(parser, variable_gt, op);
        case '=': return binop(parser, variable_assign, op);
        case OP_COLONEQUALS: return binop(parser, variable_assign, op);
        case OP_PLUSEQUALS: return binop(parser, variable_addeq, op);
        case OP_MINUSEQUALS: return binop(parser, variable_subeq, op);
        case OP_TIMESEQUALS: return binop(parser, variable_muleq, op);
        case OP_DIVEQUALS: return binop(parser, variable_diveq, op);
        case OP_MODEQUALS: return binop(parser, variable_modeq, op);
        case OP_LESSEQUALS: return binop(parser, variable_le, op);
        case OP_GREATEREQUALS: return binop(parser, variable_ge, op);
        case OP_EQUALS: return binop(parser, variable_eq, op);
        case OP_NOTEQUALS: return binop(parser, variable_neq, op);
        case OP_AND: return binop(parser, variable_and, op);
        case OP_OR: return binop(parser, variable_or, op);
        case ',': return comma(parser);
        case ';': return semicolon(parser);
        case '.': return structmbr(parser);
        case OP_UNARY_MINUS: return unop(parser, variable_neg);
        case OP_UNARY_PLUS: return unop(parser, variable_unaryplus);
        case '!': return unop(parser, variable_not);
        case OP_PREINCR: return unop(parser, variable_preincr);
        case OP_PREDECR: return unop(parser, variable_predecr);
        case OP_POSTINCR: return unop(parser, variable_postincr);
        case OP_POSTDECR: return unop(parser, variable_postdecr);
        case OP_CONTROL_FLOW: return ctrlflowbody(parser);
        case OP_ELSE: return elsebody(parser);
        case OP_USER_FUNC: return userfunc(parser);
        default: SET_PARSE_ERROR(parser, "invalid operator"); return 0;
    }
    return 1;
}

void parser_init(struct Parser* parser) {
    parser->expressions = NULL;
    parser->operators = NULL;
    parser->numExprs = 0;
    parser->numOps = 0;
    parser->status = PARSE_OPERAND;
    parser->error = NULL;
}

void parse(struct Parser* parser, const char* command) {
    struct AstNode* node;
    union Token token;
    char* next;
    char *identifier;
    int pt, pc;
    char t, c;

    if (parser->status == PARSE_ERROR || parser->status == PARSE_DONE) return;

    while ((c = next_token(command, &token, (char**)&command)) != EOF) {
        switch (c) {
            case 'I':
            case 'F':
            case 'S':
                if (parser->status != PARSE_OPERAND) {
                    SET_PARSE_ERROR(parser, "unexpected operand");
                    return;
                }
                if (!(node = push_node(parser))) return;
                parser->status = PARSE_OPERATOR;
                node->type = CONSTANT;
                switch (c) {
                    case 'I': variable_make_int(token.i, &node->data.constant.value); break;
                    case 'F': variable_make_float(token.f, &node->data.constant.value); break;
                    case 'S':
                        if (!variable_make_string(token.s, (command - 1) - token.s, &node->data.constant.value)) {
                            SET_PARSE_ERROR(parser, "memory allocation failed");
                            return;
                        }
                        break;
                }
                break;
            case 'N':
                if (parser->status == PARSE_OPERATOR && (command - token.s) == 4 && !memcmp(token.s, "else", 4)) {
                    c = OP_ELSE;
                    goto do_O;
                }
                if (parser->status != PARSE_OPERAND) {
                    SET_PARSE_ERROR(parser, "unexpected operand");
                    return;
                }
                if (!(identifier = malloc(command - token.s + 1))) {
                    SET_PARSE_ERROR(parser, "memory allocation failed");
                    return;
                }
                memcpy(identifier, token.s, command - token.s);
                identifier[command - token.s] = 0;
                if (next_token(command, NULL, &next) == '(') {
                    if (!push_op(parser, identifier)) {
                        free(identifier);
                        return;
                    }
                    if (next_token(next, NULL, &next) == ')') {
                        parser->status = PARSE_OPERATOR;
                        if (!funcall(parser, 0)) return;
                        command = next;
                    }
                } else {
                    if (!(node = push_node(parser))) {
                        free(identifier);
                        return;
                    }
                    parser->status = PARSE_OPERATOR;
                    node->type = IDENTIFIER;
                    node->data.identifier.name = identifier;
                    node->data.identifier.isDecl = 0;
                }
                break;
            case 'P':
                switch (*token.s) {
                    case '+': c = ((token.s[1] == '=') ? OP_PLUSEQUALS : ((parser->status == PARSE_OPERAND) ? OP_PREINCR : OP_POSTINCR)); break;
                    case '-': c = ((token.s[1] == '=') ? OP_MINUSEQUALS : ((parser->status == PARSE_OPERAND) ? OP_PREDECR : OP_POSTDECR)); break;
                    case '*': c = OP_TIMESEQUALS; break;
                    case '/': c = OP_DIVEQUALS; break;
                    case '%': c = OP_MODEQUALS; break;
                    case '<': c = OP_LESSEQUALS; break;
                    case '>': c = OP_GREATEREQUALS; break;
                    case '=': c = OP_EQUALS; break;
                    case '!': c = OP_NOTEQUALS; break;
                    case '&': c = OP_AND; break;
                    case '|': c = OP_OR; break;
                    case ':': c = OP_COLONEQUALS; break;
                }
                goto do_O;
            case 'O':
                c = *token.s;
                do_O:
                if (parser->status == PARSE_OPERAND) {
                    switch (c) {
                        case '-': c = OP_UNARY_MINUS; break;
                        case '+': c = OP_UNARY_PLUS; break;
                        case '!': case OP_PREDECR: case OP_PREINCR: break;
                        case ';':
                            if (!parser->numOps || *parser->operators[parser->numOps - 1] == '{' || *parser->operators[parser->numOps - 1] == OP_CONTROL_FLOW || *parser->operators[parser->numOps - 1] == OP_ELSE) {
                                if (!(node = push_node(parser))) return;
                                node->type = CONSTANT;
                                node->data.constant.value.type = VOID;
                                break;
                            }
                        default: SET_PARSE_ERROR(parser, "unexpected operator"); return;
                    }
                } else {
                    switch (c) {
                        case '!':
                            SET_PARSE_ERROR(parser, "unexpected operator");
                            return;
                    }
                }
                pc = precedence(c);
                while (parser->numOps
                    && (pt = precedence(t = (*parser->operators[parser->numOps - 1]))) >= 0
                    && (pt > pc || (pt == pc && left_associative(t)))) {
                    if (!apply_op(parser, t)) return;
                    free(parser->operators[--parser->numOps]);
                }
                if (!push_op_char(parser, c)) return;
                if (c == OP_POSTDECR || c == OP_POSTINCR) {
                    parser->status = PARSE_OPERATOR;
                } else {
                    parser->status = PARSE_OPERAND;
                }
                break;
            case '(':
                if (parser->status != PARSE_OPERAND) {
                    SET_PARSE_ERROR(parser, "unexpected '('");
                    return;
                }
                if (!push_op_char(parser, c)) return;
                break;
            case '[':
                while (parser->numOps && (t = (*parser->operators[parser->numOps - 1])) == '.') {
                    if (!apply_op(parser, t)) return;
                    free(parser->operators[--parser->numOps]);
                }
                if (parser->status != PARSE_OPERATOR) {
                    SET_PARSE_ERROR(parser, "unexpected '['");
                    return;
                }
                parser->status = PARSE_OPERAND;
                if (!push_op_char(parser, c)) return;
                break;
            case '{':
                if (parser->status != PARSE_OPERAND) {
                    SET_PARSE_ERROR(parser, "unexpected '{'");
                    return;
                }
                if (!push_op_char(parser, c)) return;
                break;
            case ')':
            case ']':
            case '}':
                if (c == '}' && parser->status == PARSE_OPERAND && parser->numOps && *parser->operators[parser->numOps - 1] == ';') {
                    if (!(node = push_node(parser))) return;
                    node->type = CONSTANT;
                    node->data.constant.value.type = VOID;
                    parser->status = PARSE_OPERATOR;
                } else if (parser->status != PARSE_OPERATOR) {
                    SET_PARSE_ERROR(parser, unexpected(c));
                    return;
                }
                while (parser->numOps && (t = (*parser->operators[parser->numOps - 1])) != '(' && t != '[' && t != '{') {
                    if (!apply_op(parser, t)) return;
                    free(parser->operators[--parser->numOps]);
                }
                if (!parser->numOps || t != matching(c)) {
                    SET_PARSE_ERROR(parser, mismatched(c));
                    return;
                }
                free(parser->operators[--parser->numOps]);
                switch (c) {
                    case ']':
                        if (!arraymbr(parser)) return;
                        break;
                    case ')':
                        if (parser->numOps) {
                            t = (*parser->operators[parser->numOps - 1]);
                            if (t == '_' || (t >= 'a' && t <= 'z') || (t >= 'A' && t <= 'Z')) {
                                if (!funcall(parser, 1)) return;
                                break;
                            }
                        }
                        if (parser->numExprs && parser->expressions[parser->numExprs - 1]->type == TUPLE) {
                            parser->expressions[parser->numExprs - 1]->data.tuple.open = 0;
                        }
                        break;
                    case '}':
                        if (parser->numExprs) {
                            if (parser->expressions[parser->numExprs - 1]->type == BLOC) {
                                parser->expressions[parser->numExprs - 1]->data.bloc.open = 0;
                            }
                            if (parser->numExprs > 1
                             && parser->expressions[parser->numExprs - 2]->type == CONTROL_FLOW
                             && parser->expressions[parser->numExprs - 2]->data.controlFlow.type != IF) {
                                c = ';';
                                goto do_O;
                            }
                        }
                        break;
                }
                break;
            default:
                SET_PARSE_ERROR(parser, "invalid token");
                return;
        }
    }
}

int parse_need_more(const struct Parser* parser) {
    unsigned int i;

    switch (parser->status) {
        case PARSE_OPERAND: return (parser->numOps > 0) && (parser->numOps > 1 || *parser->operators[parser->numOps - 1] != ';');
        case PARSE_OPERATOR: break;
        default: return 0;
    }

    if (parser->numOps == 0) return 0;
    for (i = 0; i < parser->numOps; i++) {
        if (strchr("([{", *parser->operators[i])) return 1;
    }
    switch (*parser->operators[parser->numOps - 1]) {
        case '(': case '[': case '{':
            return 1;
        case '+': case '-':
        case '*': case '/': case '%':
        case '<': case '>':
        case '=': case OP_COLONEQUALS:
        case OP_PLUSEQUALS: case OP_MINUSEQUALS:
        case OP_TIMESEQUALS: case OP_DIVEQUALS: case OP_MODEQUALS:
        case OP_LESSEQUALS: case OP_GREATEREQUALS:
        case OP_EQUALS: case OP_NOTEQUALS:
        case OP_AND: case OP_OR:
        case '.':
            return parser->numExprs < 2;
        case ',':
            return (parser->numOps >= 2 && (*parser->operators[parser->numOps - 2]) == '(') || parser->numExprs < 2;
        case ';':
            return parser->numOps >= 2;
        case OP_UNARY_MINUS: case OP_UNARY_PLUS: case '!':
        case OP_PREINCR: case OP_PREDECR:
        case OP_POSTINCR: case OP_POSTDECR:
            return parser->numExprs < 1;
        case OP_CONTROL_FLOW: case OP_ELSE: case OP_USER_FUNC:
            return parser->numExprs < 2;
    }

    return 0;
}

int parse_empty(const struct Parser* parser) {
    return parser->numExprs == 0 && parser->numOps == 0;
}

struct AstNode* parse_end(struct Parser* parser) {
    char t;
    while (parser->numOps) {
        if ((t = (*parser->operators[parser->numOps - 1])) == '(' || t == '[') {
            SET_PARSE_ERROR(parser, "')' without '('");
            return NULL;
        }
        if (!apply_op(parser, t)) return NULL;
        free(parser->operators[--parser->numOps]);
    }
    if (parser->numExprs != 1) {
        SET_PARSE_ERROR(parser, "numExpr != 1");
        return NULL;
    }
    parser->status = PARSE_DONE;
    return parser->expressions[0];
}

void parser_free(struct Parser* parser) {
    unsigned int i;

    for (i = 0; i < parser->numOps; i++) {
        free(parser->operators[i]);
    }
    free(parser->operators);

    for (i = 0; i < parser->numExprs; i++) {
        ast_free(parser->expressions[i]);
    }
    free(parser->expressions);
}

struct StackItem {
    const struct AstNode* src;
    struct AstNode** dest;
};

static int push_node_dc(struct StackItem** stack, unsigned int* stackCount, const struct AstNode* src, struct AstNode** dest) {
    struct StackItem* tmp;
    struct AstNode* node;
    if (!(node = malloc(sizeof(*node)))) return 0;
    if (!(tmp = realloc(*stack, (*stackCount + 1) * sizeof(*tmp)))) {
        free(node);
        return 0;
    }
    *stack = tmp;
    tmp += (*stackCount)++;
    tmp->dest = dest;
    tmp->src = src;
    *dest = node;
    switch (node->type = src->type) {
        case BLOC:
            node->data.bloc.count = 0;
            node->data.bloc.list = NULL;
            node->data.bloc.open = src->data.bloc.open;
            break;
        case CONTROL_FLOW:
            node->data.controlFlow.body = NULL;
            node->data.controlFlow.condition = NULL;
            node->data.controlFlow.elseBody = NULL;
            node->data.controlFlow.type = src->data.controlFlow.type;
            break;
        case IDENTIFIER:
            node->data.identifier.isDecl = src->data.identifier.isDecl;
            node->data.identifier.name = NULL;
            break;
        case FUNCTION_CALL:
            node->data.functionCall.args = NULL;
            node->data.functionCall.function = NULL;
            break;
        case BINARY_OP:
            node->data.binaryOp.a = NULL;
            node->data.binaryOp.b = NULL;
            node->data.binaryOp.op = src->data.binaryOp.op;
            break;
        case UNARY_OP:
            node->data.unaryOp.v = NULL;
            node->data.unaryOp.op = src->data.unaryOp.op;
            break;
        case ARRAY_ACCESS:
            node->data.arrayAccess.array = NULL;
            node->data.arrayAccess.index = NULL;
            break;
        case STRUCT_MEMBER:
            node->data.structMember.member = NULL;
            node->data.structMember.s = NULL;
            break;
        case TUPLE:
            node->data.tuple.count = 0;
            node->data.tuple.list = NULL;
            node->data.tuple.open = src->data.tuple.open;
            break;
        case CONSTANT:
            node->data.constant.value.type = VOID;
            break;
        case FUNCTION_DECL:
            node->data.functionDecl.arglist = NULL;
            node->data.functionDecl.body = NULL;
            break;
    }
    return 1;
}

struct AstNode* ast_deep_copy(const struct AstNode* root) {
    struct AstNode *ret, *curdest;
    const struct AstNode* cursrc;
    struct StackItem *stack = NULL, *cur;
    unsigned int numStack = 0;
    int ok = 1;

    if (!root || !push_node_dc(&stack, &numStack, root, &ret)) return NULL;
    while (ok && numStack) {
        cur = stack + (numStack - 1);
        cursrc = cur->src;
        curdest = *cur->dest;
        switch (curdest->type) {
            case BLOC:
                if (cursrc->data.bloc.count && !curdest->data.bloc.list) {
                    if (!(curdest->data.bloc.list = malloc(cursrc->data.bloc.count * sizeof(*curdest->data.bloc.list)))) {ok = 0; break;}
                }
                if (curdest->data.bloc.count == cursrc->data.bloc.count) {
                    numStack--;
                } else while (curdest->data.bloc.count < cursrc->data.bloc.count) {
                    if (!cursrc->data.bloc.list[curdest->data.bloc.count]) {
                        curdest->data.bloc.list[curdest->data.bloc.count++] = NULL;
                    } else if (push_node_dc(&stack, &numStack, cursrc->data.bloc.list[curdest->data.bloc.count], curdest->data.bloc.list + curdest->data.bloc.count)) {
                        curdest->data.bloc.count++;
                        break;
                    } else {
                        ok = 0; break;
                    }
                }
                break;
            case CONTROL_FLOW:
                if (!curdest->data.controlFlow.body && cursrc->data.controlFlow.body) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.controlFlow.body, &curdest->data.controlFlow.body)) {ok = 0; break;}
                } else if (!curdest->data.controlFlow.condition && cursrc->data.controlFlow.condition) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.controlFlow.condition, &curdest->data.controlFlow.condition)) {ok = 0; break;}
                } else if (!curdest->data.controlFlow.elseBody && cursrc->data.controlFlow.elseBody) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.controlFlow.elseBody, &curdest->data.controlFlow.elseBody)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            case IDENTIFIER:
                if (!curdest->data.identifier.name && cursrc->data.identifier.name) {
                    if (!(curdest->data.identifier.name = malloc(strlen(cursrc->data.identifier.name) + 1))) {ok = 0; break;}
                    strcpy(curdest->data.identifier.name, cursrc->data.identifier.name);
                }
                numStack--;
                break;
            case FUNCTION_CALL:
                if (!curdest->data.functionCall.args && cursrc->data.functionCall.args) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.functionCall.args, &curdest->data.functionCall.args)) {ok = 0; break;}
                } else if (!curdest->data.functionCall.function && cursrc->data.functionCall.function) {
                    if (!(curdest->data.functionCall.function = malloc(strlen(cursrc->data.functionCall.function) + 1))) {ok = 0; break;}
                    strcpy(curdest->data.functionCall.function, cursrc->data.functionCall.function);
                } else {
                    numStack--;
                }
                break;
            case BINARY_OP:
                if (!curdest->data.binaryOp.a && cursrc->data.binaryOp.a) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.binaryOp.a, &curdest->data.binaryOp.a)) {ok = 0; break;}
                } else if (!curdest->data.binaryOp.b && cursrc->data.binaryOp.b) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.binaryOp.b, &curdest->data.binaryOp.b)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            case UNARY_OP:
                if (!curdest->data.unaryOp.v && cursrc->data.unaryOp.v) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.unaryOp.v, &curdest->data.unaryOp.v)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            case ARRAY_ACCESS:
                if (!curdest->data.arrayAccess.array && cursrc->data.arrayAccess.array) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.arrayAccess.array, &curdest->data.arrayAccess.array)) {ok = 0; break;}
                } else if (!curdest->data.arrayAccess.index && cursrc->data.arrayAccess.index) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.arrayAccess.index, &curdest->data.arrayAccess.index)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            case STRUCT_MEMBER:
                if (!curdest->data.structMember.member && cursrc->data.structMember.member) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.structMember.member, &curdest->data.structMember.member)) {ok = 0; break;}
                } else if (!curdest->data.structMember.s && cursrc->data.structMember.s) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.structMember.s, &curdest->data.structMember.s)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            case TUPLE:
                if (cursrc->data.tuple.count && !curdest->data.tuple.list) {
                    if (!(curdest->data.tuple.list = malloc(cursrc->data.tuple.count * sizeof(*curdest->data.tuple.list)))) {ok = 0; break;}
                }
                if (curdest->data.tuple.count == cursrc->data.tuple.count) {
                    numStack--;
                } else while (curdest->data.tuple.count < cursrc->data.tuple.count) {
                    if (!cursrc->data.tuple.list[curdest->data.tuple.count]) {
                        curdest->data.tuple.list[curdest->data.tuple.count++] = NULL;
                    } else if (push_node_dc(&stack, &numStack, cursrc->data.tuple.list[curdest->data.tuple.count], curdest->data.tuple.list + curdest->data.tuple.count)) {
                        curdest->data.tuple.count++;
                        break;
                    } else {
                        ok = 0; break;
                    }
                }
                break;
            case CONSTANT:
                if (!variable_copy(&cursrc->data.constant.value, &curdest->data.constant.value)) {ok = 0; break;}
                numStack--;
                break;
            case FUNCTION_DECL:
                if (!curdest->data.functionDecl.arglist && cursrc->data.functionDecl.arglist) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.functionDecl.arglist, &curdest->data.functionDecl.arglist)) {ok = 0; break;}
                } else if (!curdest->data.functionDecl.body && cursrc->data.functionDecl.body) {
                    if (!push_node_dc(&stack, &numStack, cursrc->data.functionDecl.body, &curdest->data.functionDecl.body)) {ok = 0; break;}
                } else {
                    numStack--;
                }
                break;
            default:
                ok = 0;
        }
    }
    free(stack);
    if (!ok) {
        ast_free(ret);
        ret = NULL;
    }
    return ret;
}

void ast_free(struct AstNode* root) {
    struct AstNode **stack[256], **cur, **next[3], **list;
    unsigned int i, numStack = 0, numNext, *listCount;
    int hasNext;

    while (root) {
        stack[numStack++] = &root;
        while (numStack) {
            cur = stack[--numStack];
            switch ((*cur)->type) {
                case BLOC:
                    list = (*cur)->data.bloc.list;
                    listCount = &((*cur)->data.bloc.count);
                    goto do_list;
                case CONTROL_FLOW:
                    next[0] = &((*cur)->data.controlFlow.condition);
                    next[1] = &((*cur)->data.controlFlow.body);
                    next[2] = &((*cur)->data.controlFlow.elseBody);
                    numNext = 3;
                    goto do_next;
                case IDENTIFIER:
                    free((*cur)->data.identifier.name);
                    break;
                case FUNCTION_CALL:
                    if ((*cur)->data.functionCall.args) {
                        stack[numStack++] = &((*cur)->data.functionCall.args);
                        continue;
                    }
                    free((*cur)->data.functionCall.function);
                    break;
                case BINARY_OP:
                    next[0] = &((*cur)->data.binaryOp.a);
                    next[1] = &((*cur)->data.binaryOp.b);
                    numNext = 2;
                    goto do_next;
                case UNARY_OP:
                    if ((*cur)->data.unaryOp.v) {
                        stack[numStack++] = &((*cur)->data.unaryOp.v);
                        continue;
                    }
                    break;
                case ARRAY_ACCESS:
                    next[0] = &((*cur)->data.arrayAccess.array);
                    next[1] = &((*cur)->data.arrayAccess.index);
                    numNext = 2;
                    goto do_next;
                case STRUCT_MEMBER:
                    next[0] = &((*cur)->data.structMember.s);
                    next[1] = &((*cur)->data.structMember.member);
                    numNext = 2;
                    goto do_next;
                case TUPLE:
                    list = (*cur)->data.tuple.list;
                    listCount = &((*cur)->data.tuple.count);
                    goto do_list;
                case CONSTANT:
                    variable_free(&(*cur)->data.constant.value);
                    break;
                case FUNCTION_DECL:
                    next[0] = &((*cur)->data.functionDecl.arglist);
                    next[1] = &((*cur)->data.functionDecl.body);
                    numNext = 2;
                    goto do_next;
                do_next:
                    hasNext = 0;
                    for (i = 0; i < numNext; i++) {
                        if (*next[i]) {
                            hasNext = 1;
                            if (numStack < sizeof(stack) / sizeof(*stack)) {
                                stack[numStack++] = next[i];
                            }
                        }
                    }
                    if (hasNext) continue;
                    break;
                do_list:
                    if (!*listCount) {
                        free(list);
                        break;
                    }
                    i = 0;
                    while (i < *listCount && numStack < sizeof(stack) / sizeof(*stack)) {
                        if (list[i]) {
                            stack[numStack++] = list + i;
                            i++;
                        } else {
                            list[i] = list[--(*listCount)];
                        }
                    }
                    continue;
            }
            free(*cur);
            *cur = NULL;
        }
    }
}
