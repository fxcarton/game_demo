#include "shell.h"

#ifndef TDSH_RANDOM_H
#define TDSH_RANDOM_H

int shell_make_random_functions(struct DemoShell* shell);

struct MTState;
int variable_is_mtstate(const struct Variable* v);
struct MTState* variable_to_mtstate(const struct Variable* v);

#endif
