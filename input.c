#include <stdio.h>
#include <stdlib.h>
#ifdef READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include "completion.h"
#include "shell.h"

void* shell_input_thread(void* data) {
    char buffer[2048];
    struct DemoShell* shell = data;
    FILE* f = NULL;
    const char *prompt, *curFileName = "", *curLine;
    char* line = NULL;
    unsigned int curLineNo = 0;
    int running = 1, interactive = 0, fileDone = 0;

    do {
        if (pthread_mutex_lock(&shell->inputMutex)) {
            continue;
        }
        while (shell->curLine) {
            pthread_cond_wait(&shell->inputCond, &shell->inputMutex);
        }
        prompt = shell->prompt;
        pthread_mutex_unlock(&shell->inputMutex);
        if (shell->config.execCmd) {
            curLine = shell->config.execCmd;
            shell->config.execCmd = NULL;
            curFileName = "<CLI argument>";
            curLineNo = 0;
        } else {
#ifdef READLINE
            if (interactive) free(line);
#endif
            line = NULL;
            while (running && !line) {
                interactive = (f == stdin);
                if (f) {
#ifdef READLINE
                    if ((line = (interactive ? readline(prompt) : fgets(buffer, sizeof(buffer), f)))) {
                        if (*line && interactive) add_history(line);
                    } else {
#ifndef READLINE
                    }
#endif
#else
                    if (f == stdin) {
                        printf("%s", prompt);
                        fflush(stdout);
                    }
                    if (!(line = fgets(buffer, sizeof(buffer), f))) {
#endif
                        if (f == stdin) {
                            printf("exit()\n");
                            running = 0;
                        } else {
                            fclose(f);
                            f = NULL;
                        }
                    }
                    curLineNo++;
                } else {
                    if (shell->config.argc > 0 && !fileDone) {
                        fileDone = 1;
                        if (!(f = fopen(shell->config.argv[0], "r"))) {
                            fprintf(stderr, "Warning: failed to open '%s'\n", shell->config.argv[0]);
                        } else {
                            curFileName = shell->config.argv[0];
                            curLineNo = 0;
                        }
                    } else {
                        curFileName = "-";
                        shell_init_completion(shell);
                        f = stdin;
                    }
                }
            }
            curLine = line;
        }
        if (running) {
            if (!pthread_mutex_lock(&shell->inputMutex)) {
                pthread_cond_signal(&shell->mainCond);
                shell->interactive = interactive;
                shell->curLine = curLine;
                shell->curLineNo = curLineNo;
                shell->curFile = curFileName;
                pthread_mutex_unlock(&shell->inputMutex);
            }
        }
        if (!pthread_mutex_trylock(&shell->mainMutex)) {
            shell->running = (running = running && shell->running);
            pthread_mutex_unlock(&shell->mainMutex);
        }
    } while (running);
    if (!pthread_mutex_lock(&shell->mainMutex)) {
        shell->running = (running = running && shell->running);
        pthread_mutex_unlock(&shell->mainMutex);
    }
    if (f) fclose(f);
    return NULL;
}
