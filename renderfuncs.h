#include "shell.h"

#ifndef TDSH_FUNCTIONS_RENDER_H
#define TDSH_FUNCTIONS_RENDER_H

int shell_make_render_functions(struct DemoShell* shell);

#endif
