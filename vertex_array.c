#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <3dmr/mesh/mesh.h>
#include "builtins.h"
#include "mesh.h"
#include "shell.h"
#include "render.h"

#define VA_MEMBER(name) \
static int va_##name(const void* src, struct Variable* dest) { \
    const struct VertexArray* va = ((struct ShellRefCount*)src)->data; \
    variable_make_int(va->name, dest); \
    return 1; \
}

#define VA_ATTR(name) \
static int va_##name(const void* src, struct Variable* dest) { \
    const struct VertexArray* va = ((struct ShellRefCount*)src)->data; \
    variable_make_int(!!(va->flags & MESH_##name), dest); \
    return 1; \
}

VA_MEMBER(numVertices)
VA_MEMBER(numIndices)
VA_MEMBER(vao)
VA_MEMBER(vbo)
VA_MEMBER(ibo)
VA_ATTR(NORMALS)
VA_ATTR(TEXCOORDS)
VA_ATTR(TANGENTS)

static const struct FieldInfo structVertexArrayFields[] = {
    {"numVertices", va_numVertices},
    {"numIndices", va_numIndices},
    {"vao", va_vao},
    {"vbo", va_vbo},
    {"ibo", va_ibo},
    {"hasNormals", va_NORMALS},
    {"hasTexcoords", va_TEXCOORDS},
    {"hasTangents", va_TANGENTS},
    {0}
};

static const struct StructInfo structVertexArray;

static int va_copy_(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(&structVertexArray, data, dest);
    return 1;
}

static void va_free_(void* data) {
    shell_decref(data);
}

static const struct StructInfo structVertexArray = {"VertexArray", structVertexArrayFields, va_copy_, va_free_};

static void va_destroy_(struct DemoShell* shell, void* d) {
    if (!d) return;
    if (!pthread_mutex_lock(&shell->swapMutex)) {
        viewer_make_current(shell->viewer);
        vertex_array_del(d);
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    free(d);
}

static struct ShellRefCount* shell_new_va(struct DemoShell* shell, const struct Mesh* mesh) {
    struct ShellRefCount* r;
    struct VertexArray* va = NULL;

    if (!(r = shell_new_refcount(shell, va_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return NULL;
    }
    if (!(va = malloc(sizeof(*va)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return NULL;
    }
    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
        shell_decref(r);
        free(va);
        return NULL;
    }
    viewer_make_current(shell->viewer);
    vertex_array_gen(mesh, va);
    viewer_make_current(NULL);
    pthread_mutex_unlock(&shell->swapMutex);
    r->data = va;
    return r;
}

int vertex_array_from_mesh(struct DemoShell* shell, const struct Mesh* mesh, struct Variable* dest) {
    struct ShellRefCount* r;

    if (!(r = shell_new_va(shell, mesh))) {
        return 0;
    }
    variable_make_struct(&structVertexArray, r, dest);
    return 1;
}

int variable_make_vertex_array(struct DemoShell* shell, struct VertexArray* vertexArray, struct Variable* dest) {
    struct ShellRefCount* r;
    struct VertexArray* va = NULL;
    unsigned int i;

    for (i = 0; i < shell->numRefcounts; i++) {
        if (shell->refcount[i] && shell->refcount[i]->destroy == va_destroy_ && (va = shell->refcount[i]->data) && va->vao == vertexArray->vao) {
            if (va->vbo != vertexArray->vbo || va->ibo != vertexArray->ibo
             || va->numVertices != vertexArray->numVertices || va->numIndices != vertexArray->numIndices
             || va->flags != vertexArray->flags
             || !shell_incref(shell->refcount[i])) {
                return 0;
            }
            variable_make_struct(&structVertexArray, shell->refcount[i], dest);
            return 1;
        }
    }
    if (!(r = shell_new_refcount(shell, va_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(va = malloc(sizeof(*va)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return 0;
    }
    *va = *vertexArray;
    r->data = va;

    variable_make_struct(&structVertexArray, r, dest);
    return 1;
}

static int vertex_array(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable tmp;
    int ptr, r = 0;

    if (nArgs != 1) {
        fprintf(stderr, "Error: compute_tangents() needs one argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &tmp);
    if (!variable_is_mesh(&tmp)) {
        fprintf(stderr, "Error: vertex_array() needs one mesh argument\n");
    } else if (!vertex_array_from_mesh(data, variable_to_mesh(&tmp), ret)) {
        fprintf(stderr, "Error: failed to create vertex array from mesh\n");
    } else {
        r = 1;
    }
    if (ptr) variable_free(&tmp);
    return r;
}

int shell_make_vertex_array_functions(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "vertex_array", vertex_array, shell);
}

int variable_is_vertex_array(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structVertexArray;
}

struct VertexArray* variable_to_vertex_array(struct Variable* var) {
    if (variable_is_vertex_array(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}
