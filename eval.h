#ifndef TDSH_EVAL_H
#define TDSH_EVAL_H

struct EvalState {
    struct DemoShell* shell;
    struct Variable** expressions;
    struct AstNode** operators;
    unsigned int numExprs, numOps, allocExprs, allocOps;
    int ok;
};

int eval_node_start(const struct AstNode* root, struct DemoShell* shell, struct EvalState* state);
int eval_node_iter(struct EvalState* state);
int eval_node_end(struct EvalState* state, struct Variable* dest);

#endif
