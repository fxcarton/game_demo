#include "shell.h"

#ifndef TDSH_FUNCTIONS_SYS_H
#define TDSH_FUNCTIONS_SYS_H

int shell_make_sys_functions(struct DemoShell* shell);

#endif
